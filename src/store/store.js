import { makeAutoObservable } from "mobx";

class InventarsStore {
  cabinets = [];
  sprnames = [];
  sprequipments = [];

  constructor() {
    makeAutoObservable(this);
  }

  fetchPosts = async () => {            
    try {
      const response = await fetch(`https://seric0.kz/API/inventar/getCabinet.php`);
      const data = await response.json();
      this.cabinets = data;
    } catch (error) {      
      console.error("Error fetching cabinets:", error);
    }
  };

  fetchNames = async () => {          
    try {
      const response = await fetch(`http://seric0.kz/API/inventar/getSprNames.php`);
      const data = await response.json();
      this.sprnames = data;
    } catch (error) {      
      console.error("Error fetching sprnames:", error);
    }
  };

  fetchSprs = async () => {          
    try {
      const response = await fetch(`http://seric0.kz/API/inventar/getSprMainEquipment.php`);
      const data = await response.json();
      this.sprequipments = data;
    } catch (error) {      
      console.error("Error fetching sprequipments:", error);
    }
  };
   
   
}

const inventarsStore = new InventarsStore();
export default inventarsStore;