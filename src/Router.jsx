import React from "react";

import {BrowserRouter, Routes, Route} from "react-router-dom";
import HomePage from "./pages/HomePage";
import MainInventoryPage from "./pages/MainInventoryPage";
import EquipmentPage from "./pages/EquipmentPage";
import ReferencesPage from "./pages/ReferencesPage";
import ContactPage from "./pages/ContactPage";
import ReportPage from "./pages/ReportPage";
import Page_404 from "./pages/Page_404";

function Router() {
  
  return (    
    <BrowserRouter>
    <Routes>    
      <Route path="/" element={<HomePage />} />      
      <Route path="/inventar" element={<MainInventoryPage />} />      
      <Route path="/otherinventar" element={<EquipmentPage />} />      
      <Route path="/spr" element={<ReferencesPage />} />
      <Route path="/contact" element={<ContactPage />} />
      <Route path="/report" element={<ReportPage />} />
      <Route path="*" element={<Page_404 />} />      
    </Routes>
    </BrowserRouter>
  //   <BrowserRouter>
  //   <Routes>
  //   {isLoadingPage && 
  //       <Route path="*" element={<LoadingPage />} />
  //     }
  //     {user && <Route path="/" element={<HomePage />} />}
  //     {isAuthorizationPage && (
  //       <Route path="*" element={<AuthorizationPage />} />
  //     )}
  //     {/* <Route path="*" element={<UsersPage />} /> */}
  //   </Routes>
  // </BrowserRouter>
  );
}

export default Router;