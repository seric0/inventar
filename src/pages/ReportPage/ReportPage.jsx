import React from "react";

import Header from "../../components/Header/Header";
import Reports from "../../components/Reports/Reports";
import Footer from "../../components/Footer/Footer";

import reports from "./ReportPage.module.scss"

function ReportPage() {  
  return (
    <div className={reports}>
       <Header/>
       <Reports/>
       <Footer/>     
    </div>    
  );
}

export default ReportPage;
