import React from "react";

import Header from "../../components/Header/Header";
import Home404 from "../../components/Home_404/Home_404";
import Footer from "../../components/Footer/Footer";

import Page_404_Error from "./Page_404.module.scss"

function Page_404() {  
  return (
    <div className={Page_404_Error}>
       <Header/>
       <Home404/>
       <Footer/>     
    </div>    
  );
}

export default Page_404;
