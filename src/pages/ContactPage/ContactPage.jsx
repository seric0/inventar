import React from "react";

import Header from "../../components/Header/Header";
import Contacts from "../../components/Contacts/Contacts";
import Footer from "../../components/Footer/Footer";

import contacts from "./ContactPage.module.scss"

function ContactPage() {  
  return (
    <div className={contacts}>
       <Header/>
       <Contacts/>
       <Footer/>     
    </div>    
  );
}

export default ContactPage;
