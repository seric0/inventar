import React from "react";

import Header from "../../components/Header/Header";
import HomeStatic from "../../components/HomeStatic/HomeStatic";
import Footer from "../../components/Footer/Footer";

import s from "./HomePage.module.scss"

function HomePage() {  
  return (
    <div>
      <div className={s.Header}>
      {/* {headerMenu.map((linkName) => (
              <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}       */}
        <Header/>  
      </div>
      <div className={s.HomeStatic}>
      <HomeStatic/>   
      </div>
      <div className={s.Footer}>
      <Footer/>   
      </div>     
      </div>    
  );
}

export default HomePage;
