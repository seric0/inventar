import React from "react";
// import { useSelector, useDispatch } from 'react-redux';
// import { getPosts } from "../../store/postSlice";

import Header from "../../components/Header/Header";
import MainInventory from "../../components/MainInventory/MainInventory";
// import PopularCard from "../../components/PopularCard/PopularCard";
// import StockCard from "../../components/StockCard/StockCard";
// import HomeCoach from "../../components/HomeCoach/HomeCoach";
import Footer from "../../components/Footer/Footer";

import s from "./MainInventoryPage.module.scss"

// let headerMenu = [
//   {
//     id: 1,
//     img: LogoImage,
//     menu_1: "Главная",    
//     menu_2:"Бани и сауны",        
//     menu_3:"Галерея",    
//     menu_4:"Контакты"
//     }
//   ];

function MainInventoryPage() {  
  // const { post } = useSelector((state) => state.post);
  // const [page, setPage] = useState(1);  
  // const dispatch = useDispatch();
  
  // useEffect(() => {
  //   dispatch(getPosts({  }));
  // }, [dispatch]);
  return (
    <div>
      <div className={s.Header}>
      {/* {headerMenu.map((linkName) => (
              <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}       */}
        <Header/>  
      </div>
      <div className={s.MainInventory}>
      {/* {post?.map((card) => (
      <MainInventory name={card.name} inv_number={card.inv_number} serial_number={card.serial_number} cabinet={card.cabinet}/>
      ))}    */}
      <MainInventory/>
      </div>
      <div className={s.Footer}>
      <Footer/>   
      </div>
      {/* <div className={s.list}>
      <p className={s.text}>Популярные</p>            
            <PopularCard/>          
      </div>
      <div className={s.list}>
      <p className={s.text}>Акции и спецпредложения</p>      
            <StockCard/>           
      </div>
      <div className={s.HomeCoach}>
      <HomeCoach/>   
      </div>      
      <div className={s.Footer}>
      {headerMenu.map((linkName) => (
              <Footer menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>     */}
      </div>    
  );
}

export default MainInventoryPage;
