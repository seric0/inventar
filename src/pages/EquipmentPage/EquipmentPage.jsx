import React from "react";

import Header from "../../components/Header/Header";
import MainEquipment from "../../components/MainEquipment/MainEquipment";
import Footer from "../../components/Footer/Footer";

import s from "./EquipmentPage.module.scss"

function EquipmentPagePage() {    
      
  return (
    <div>
      <div className={s.Header}>   
        <Header/>  
      </div>
      <div className={s.EquipmentPage}>      
      <MainEquipment/>
      </div>
      <div className={s.Footer}>
      <Footer/>   
      </div>      
      </div>    
  );
}

export default EquipmentPagePage;
