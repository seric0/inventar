import React from "react";

import Header from "../../components/Header/Header";
import MainReferences from "../../components/MainReferences/MainReferences";
import Footer from "../../components/Footer/Footer";

import s from "./ReferencesPage.module.scss"

function ReferencesPage() {    
      
  return (
    <div>
      <div className={s.Header}>   
        <Header/>  
      </div>
      <div className={s.ReferencesPage}>      
      <MainReferences/>
      </div>
      <div className={s.Footer}>
      <Footer/>   
      </div>      
      </div>    
  );
}

export default ReferencesPage;
