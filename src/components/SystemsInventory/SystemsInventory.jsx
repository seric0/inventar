import React, { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import { useSnackbar } from "notistack";
import TabPanel from '@mui/lab/TabPanel';
import { FaFolderPlus } from "react-icons/fa";
import { FaEdit } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaDesktop } from "react-icons/fa";
import { FaSave } from "react-icons/fa";
import { FaFileExcel } from "react-icons/fa";
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import * as XLSX from 'xlsx';

import systemsinventar from "./SystemsInventory.module.scss"

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  padding: '33px',
};

function SystemsInventory() {        

        var indexthree = 1;               
        const [openInv, setOpenInv] = useState(false); 
        const [currentPostInv, setCurrentPostInv] = useState(null);
        const [searchparam, setSearchParam] = useState('2');       
        const handleCloseInv = () => setOpenInv(false); 
        const [systems, setSystems] = useState([]); 
        const [page, setPage] = useState(1); 
        const [filteredSystems, setFilteredSystems] = useState([]); 
        const [searchtext, setSearchText] = useState('');
        const [open, setOpen] = useState(false);
        const [opencomp, setOpenComp] = useState(false);
        const handleOpen = () => setOpen(true);
        const handleClose = () => setOpen(false);
        const handleCloseComp = () => setOpenComp(false);
        const [manufacturer_id, setManufacturer] = useState('');        
        const [selectedId, setSelectedId] = useState(null);
        const [name, setName] = useState('');        
        const [inv, setInv] = useState('');
        const [serial, setSerial] = useState('');
        const [cab, setCab] = useState('');        
        const [invtype, setInvType] = useState('');
        const [model, setModel] = useState('');        
        const [work, setWork] = useState('');
        const [status, setStatus] = useState('');
        const [property_one, setProc] = useState('');        
        const [property_two, setFreq] = useState('');
        const [property_three, setMem] = useState('');
        const [property_four, setHdd] = useState('');
        const [property_five, setOC] = useState('');
        const [isDataAvailable, setIsDataAvailable] = useState(false);
                
        const { enqueueSnackbar } = useSnackbar();

        const [spectone, setSpecOne] = useState([]);
          const fetchSpecData = (id) => {                          
              fetch(`https://seric0.kz/API/inventar/getSpecOne1.php?idcomp=${id}`)
                .then((response) => {
                  return response.json();                                                      
                })
                .then((data) => {
                  setSpecOne(data);                  
                });            
          };
          useEffect(() => {
            fetchSpecData();            
          }, []);
          
          const [names, setNames] = useState([]);
            const fetchDataSprName = () => {
              fetch(`http://seric0.kz/API/inventar/getSprNames.php`)
                .then((response) => {
                  return response.json();
                })
                .then((data) => {
                  setNames(data);
                });
            };
            useEffect(() => {
              fetchDataSprName();
            }, []);
          
            const [spr_manufacturer, setSprManufacturer] = useState([]);
            const fetchDataSprManufacturer = () => {
              fetch(`https://seric0.kz/API/inventar/getSprManufacturer.php`)
                .then((response) => {
                  return response.json();
                })
                .then((data) => {
                  setSprManufacturer(data);
                });
            };
            useEffect(() => {
              fetchDataSprManufacturer();
            }, []);

  const [cabinet, setCabinet] = useState([]);
  const fetchDataCab = () => {
    fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setCabinet(data);
      });
  };
  useEffect(() => {
    fetchDataCab();
  }, []);          

  const [spr, setSpr] = useState([]);
  const fetchDataSpr = () => {
    fetch(`http://seric0.kz/API/inventar/getSprMain.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSpr(data);
      });
  };
  useEffect(() => {
    fetchDataSpr();
  }, []);

  const [frequency, setFrequency] = useState([]);        
          const fetchFrequencySpr = () => {            
            fetch(`https://seric0.kz/API/inventar/getFrequency.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setFrequency(data);
              });
          };        
          useEffect(() => {
            fetchFrequencySpr();
          }, []);
  
          const [memoryspr, setMemorySpr] = useState([]);   
          const fetchMemorySpr = () => {            
            fetch(`https://seric0.kz/API/inventar/getMemory.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setMemorySpr(data);
              });
          };        
          useEffect(() => {
            fetchMemorySpr();
          }, []);
  
          const [hddspr, setHddSpr] = useState([]);   
          const fetchHddSpr = () => {            
            fetch(`https://seric0.kz/API/inventar/getHdd.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setHddSpr(data);
              });
          };        
          useEffect(() => {
            fetchHddSpr();
          }, []);
  
          const [ocspr, setOcSpr] = useState([]);   
          const fetchOcSpr = () => {            
            fetch(`https://seric0.kz/API/inventar/getOC.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setOcSpr(data);
              });
          };        
          useEffect(() => {
            fetchOcSpr();
          }, []);

          const [processors, setProcessors] = useState([]);
                    const fetchDataProcessor = () => {            
                      fetch(`https://seric0.kz/API/inventar/getProcessor.php`)
                        .then((response) => {
                          return response.json();
                        })
                        .then((data) => {
                          setProcessors(data);
                        });
                    };        
                    useEffect(() => {
                      fetchDataProcessor();
                    }, []);
                                              
        const openData = (id) => {          
          setSelectedId(id);          
          setOpen(true);          
        }

        const openComp = (id) => {                   
          fetchSpecData(id);                           
          setOpenComp(true);                    
        }

        const saveData = () => {        
          var params = new URLSearchParams();
          params.set('property_one', property_one);
          params.set('property_two', property_two);
          params.set('property_three', property_three);
          params.set('property_four', property_four);
          params.set('property_five', property_five);        
          params.set('idcomp', selectedId);
        if (property_one !== "" && property_two !== "" && property_three !=="" && property_four !=="") { 
        fetch(`https://seric0.kz/API/inventar/addSpecifications.php`, {
          method: 'POST',
          body: params
       }).then(
          response => {
             return response.text();       
          }  
       ).then(
          text => {                  
            enqueueSnackbar({
              variant: "success",
              message: "Данные добавлены",
            });          
            setTimeout(window.location.reload(), 2000);
            handleClose();       
          }           
       ); 
      }
      else {
        enqueueSnackbar({
          variant: "error",
          message: "Введите все данные!",
        }); 
      }
          }
                        
          const fetchDataSystem = () => {            
            fetch(`http://seric0.kz/API/inventar/getInventar_system.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setSystems(data);
              });
          };        
          useEffect(() => {
            fetchDataSystem();
          }, []);

          const exportToExcel = () => {
                        var excelindex = 1;
                        const headers = ['№ п/п', 'Наименование', 'Модель', 'Инвентарный номер', 'Серийный номер', '№ кабинета', 'Статус работоспособности', 'Причина'];                      
                        const data = systems.map(item => [excelindex++, item.name_inv, item.name_spr+' '+item.model,item.inv_number, item.serial_number,item.name, item.work, item.status]);                        
                        const ws = XLSX.utils.aoa_to_sheet([headers, ...data]);          
                        const wb = XLSX.utils.book_new();
                        XLSX.utils.book_append_sheet(wb, ws, 'Копьютеры');          
                        XLSX.writeFile(wb, 'data.xlsx');
                      };
          
          const [postPerPage] = useState(10);  
          const lastPost = page * postPerPage;
          const firstPost = lastPost - postPerPage;          
          const currentPost = filteredSystems.slice(firstPost, lastPost);
          const PageCount = Math.ceil(filteredSystems.length / postPerPage);          
          const ChangePage = ( event, value ) => {                                        
            setPage(value);          
          };

          const findData = () => {                                                                                                                     
            if (searchtext) {
              let filtered = [];

              if (searchparam === '1') {
                filtered = systems.filter((system) => system.name_inv.toLowerCase().includes(searchtext.toLowerCase()));
              }
              
              if (searchparam === '2') {
                filtered = systems.filter((system) => system.inv_number.includes(searchtext));
              }
            
              if (searchparam === '3') {
                filtered = systems.filter((system) => system.serial_number.includes(searchtext));
              }

              if (searchparam === '4') {
                filtered = systems.filter((system) => system.name.includes(searchtext));
              }
              
              setFilteredSystems(filtered);
            } else {
              setFilteredSystems(systems);
            }
          }

          useEffect(() => {
          findData();
          }, [systems]); 

          const saveDataInv = () => { 
            const currentDate = new Date();
            const date_inv = currentDate.toISOString().slice(0, 19).replace('T', ' '); 
      
            const updatedData = {
              id: currentPostInv.id,  
              name_inventar_id: name,        
              model: model,
              inv_number: inv,
              serial_number: serial,
              cabinet: cab,
              manufacturer_id: manufacturer_id,
              inventar_type: invtype,
              work: work,
              status: status,
              date_inv: date_inv   
            };
                   
            fetch(`https://seric0.kz/API/inventar/updateInventar.php`, {
              method: 'PUT',
              body: JSON.stringify(updatedData),
              headers: {
                'Content-type': 'application/json; charset=UTF-8',
              },    
            })
              .then((response) => response.json())
              .then(() => {                        
                  enqueueSnackbar({
                    variant: "success",
                    message: "Данные обновлены",
                  });
                //setTimeout(window.location.reload(), 4000);          
                fetchDataSystem();
                handleCloseInv();        
              })
              .catch((error) => {
                enqueueSnackbar({
                  variant: "error",
                  message: "Ошибка при обновлении "+error,
                })
              });      
          };

          const openInvData = (post) => {
            setCurrentPostInv(post);
            setName(post.name_inventar_id);
            setManufacturer(post.manufacturer_id);
            setModel(post.model);                
            setInv(post.inv_number);
            setSerial(post.serial_number);
            setCab(post.cabinet);                        
            setInvType(post.inventar_type);
            setWork(post.work);
            setStatus(post.status);                        
            setOpenInv(true);
          }        
                            
    return (                                        
        <div className={systemsinventar.main}>                                 
        <TabPanel value="3" sx={{ width: '1024px' }}>
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите инвентарный номер" className={systemsinventar.searchinput} value={searchtext} onChange={event => setSearchText(event.target.value)}/>
        </label>
        <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={searchparam}
                  label="Type"
                  sx={{ marginLeft: '15px' }}
                  onChange={event => setSearchParam(event.target.value)}
                >                  
                    <MenuItem value="1">Наименование</MenuItem>
                    <MenuItem value="2">Инвентарный номер</MenuItem>
                    <MenuItem value="3">Серийный номер</MenuItem>
                    <MenuItem value="4">№ кабинета</MenuItem>                    
        </Select>
        <Button variant="contained" title="Экспорт в excel" onClick={exportToExcel} sx={{ marginLeft: '20px', height: '30px'}}><FaFileExcel /></Button>
        <Button variant="contained" title="Поиск" onClick={findData} sx={{ marginLeft: '20px', height: '30px' }}><FaSearch /></Button>
        <Modal
          open={openInv}
          onClose={handleCloseInv}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description">
          <Box sx={style}>
            <Button onClick={handleCloseInv} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
            <form id="form" className={systemsinventar.input_forms}>
              <label>Наименование:</label>              
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={name}
                label="Type"
                onChange={event => setName(event.target.value)}
              >
                {names.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <InputLabel id="demo-simple-select-label">Производитель:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={manufacturer_id}
                label="Type"
                onChange={event => setManufacturer(event.target.value)}
              >
                {spr_manufacturer.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr}</MenuItem>
                ))}
              </Select>
              <label>Модель:</label>
              <input type="text" value={model} className={systemsinventar.textinput} name="model" id="model" required onChange={event => setModel(event.target.value)} />
              <label>Инвентарный номер:</label>             
              <input type="text" value={inv} className={systemsinventar.textinput} name="inv_number" id="inv_number" required onChange={event => setInv(event.target.value)} />              
              <label>Серийный номер:</label>
              <input type="text" value={serial} className={systemsinventar.textinput} name="serial_number" id="serial_number" required onChange={event => setSerial(event.target.value)} />
              <label>№ кабинета:</label>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >
                  {cabinet.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name}</MenuItem>
                  ))}
                </Select>
              <InputLabel id="demo-simple-select-label">Тип:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={invtype}
                label="Type"
                onChange={event => setInvType(event.target.value)}
              >
                {spr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={work}
                  label="Type"
                  onChange={event => setWork(event.target.value)}
                >
                  <MenuItem value="Работает">Работает</MenuItem>
                  <MenuItem value="Не работает">Не работает</MenuItem>
                </Select>
                <label>Причина:</label>
                            <textarea value={status} className={systemsinventar.textinput} name="status" id="status" required onChange={event => setStatus(event.target.value)}/>  
              <Button variant="contained" title="Сохранить" onClick={saveDataInv} sx={{ height: '30px' }}><FaSave /></Button>
            </form>
          </Box>
        </Modal> 
        <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>                                          
                      <form id="form" className={systemsinventar.input_forms}>                                                                          
                            <InputLabel id="demo-simple-select-label">Наименование процессора:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={property_one}
                label="Type"
                onChange={event => setProc(event.target.value)}
              >
                {processors.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_processor}</MenuItem>
                ))}
              </Select>
              <InputLabel id="demo-simple-select-label">Тактовая частота:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={property_two}
                label="Type"
                onChange={event => setFreq(event.target.value)}
              >
                {frequency.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr} ГГц</MenuItem>
                ))}
              </Select>        
              <InputLabel id="demo-simple-select-label">Оперативная память:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={property_three}
                label="Type"
                onChange={event => setMem(event.target.value)}
              >
                {memoryspr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr > 100 ? post.name_spr + " Мб" : post.name_spr + " Гб"}</MenuItem>
                ))}
              </Select>       
              <InputLabel id="demo-simple-select-label">Жесткий диск:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={property_four}
                label="Type"
                onChange={event => setHdd(event.target.value)}
              >
                {hddspr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr > 100 ? post.name_spr + " Гб" : post.name_spr + " Тб"}</MenuItem>
                ))}
              </Select>   
              <InputLabel id="demo-simple-select-label">Программное обеспечение:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={property_five}
                label="Type"
                onChange={event => setOC(event.target.value)}
              >
                {ocspr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr}</MenuItem>
                ))}
              </Select>                          
                                                                                                                                                                                                   
                            <Button variant="contained" onClick={saveData}>Сохранить</Button>                                                        
                        </form>                                                                       
                    </Box>
                </Modal>
                <Modal
                    open={opencomp}
                    onClose={handleCloseComp}
                    aria-labelledby="parent-modal-title"
                    aria-describedby="modal-modal-description">                    
                    <Box sx={style}>
                    <Button onClick={handleCloseComp} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
                    {spectone.map((comp) => (                      
                      <div key={comp.id} className={systemsinventar.specification}>
                      <h1 className={systemsinventar.specifications}>Харатеристики компьютера</h1>
                      <p>Наименование процессора: {comp.property_one_name || 'Не указано'}</p>
                      <p style={{ color: comp.property_two_name <= 2 ? 'red' : 'black' }}>Частота процессора: {comp.property_two_name || 'Не указано'} Гц</p>                      
                      <p style={{ color: comp.property_three_name <= 2 ? 'red' : 'black' }}>Количество оперативной памяти: {comp.property_three_name || 'Не указано'} Гб</p>
                      <p style={{ color: comp.property_four_name < 250 ? 'red' : 'black' }}>Объем жесткого диска: {comp.property_four_name || 'Не указано'} Гб</p>
                      <p style={{ color: ['Windows XP','Windows 7', 'Windows 8', 'Windows 8.1'].includes(comp.property_five_name) ? 'red' : 'black' }}>Операционная система: {comp.property_five_name || 'Не указано'}</p>                                        
                      </div>                                                                
                    ))}                       
                    </Box>                  
                </Modal>  
        <Stack spacing={2}>           
        {systems.length > 0 && (        
        <table id="info-table">
          <thead>
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>
                <th>Модель</th>
                <th>Инвентарный номер</th>
                <th>Серийный номер</th>
                <th>№ кабинета</th>
                <th>Статус работоспособности</th>
                <th>Причина</th>
            </tr>
          </thead>
            <tbody id="tbody">                                    
            {currentPost.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexthree++}</td>
                <td>{post.name_inv || 'Не указано'}</td>                                                
                <td>{post.name_spr+' '+post.model || 'Не указано'}</td>
                <td>{post.inv_number || 'Не указано'}</td>
                <td>{post.serial_number || 'Не указано'}</td>
                <td>{post.name || 'Не указано'}</td>
                <td>{post.work || 'Не указано'}</td>
                <td>{post.status || 'Не указано'}</td>
                <td><Button variant="contained" title="Редактировать" onClick={() =>{openInvData(post)}}><FaEdit /></Button></td>                
                <td><Button variant="contained" disabled={isDataAvailable} title="Добавить характеристики" onClick={() => openData(post.id)}><FaFolderPlus /></Button></td>
                <td><Button variant="contained" title="Карточка основного средства" onClick={() => openComp(post.id)}><FaDesktop /></Button></td>
            </tr>            
            ))}
            </tbody>                      
        </table>        
        )} 
        </Stack>
        <Pagination
        count={PageCount}        
        showFirstButton
        showLastButton               
        onChange={ChangePage}                 
        disableInitialCallback={true}        
        initialPage={1}        
        color="primary"
        size="large"        
      /> 
      </TabPanel>
    </div>        
    );
}

export default SystemsInventory;
