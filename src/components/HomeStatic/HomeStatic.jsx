import React, { useState,useEffect } from "react";
import LoadingScreen from "../Loader/Loader";
import { useSnackbar } from "notistack";

import homestatic from "./HomeStatic.module.scss"


function HomeStatic() {  
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isModal2Open, setIsModal2Open] = useState(false);
    const [comps, setComps] = useState([]);
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);    
    const { enqueueSnackbar } = useSnackbar();

    const [formData, setFormData] = useState({login: "",passwd: ""});
    const [login, setLogin] = useState('');
    const [passwd, setPasswd] = useState('');
    const [fio, setFio] = useState('');    
    const [post, setPost] = useState('');
      
    useEffect(() => {
      const login = localStorage.getItem("fio");
      if (login == null)
      setIsModalOpen(true);
    }, []);
    useEffect(() => {
      setTimeout(() => setLoading(false), 2000);
    }, [])      
    const fetchDataComp = () => {            
      fetch(`http://seric0.kz/API/inventar/getInventar_count.php`)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setComps(data);
        });
    };        
    useEffect(() => {
      fetchDataComp();
    }, []);

    // const closeModal = () => {          
    //   setIsModalOpen(false);      
    // };     

    const regModal = () => {          
      setIsModalOpen(false);
      setIsModal2Open(true);      
    };  

    const authModal = () => {          
      setIsModalOpen(true);
      setIsModal2Open(false);      
    }; 

    //login
    //passwd
    //fio
    //post
const fetchDataUsers = () => {            
            fetch(`https://seric0.kz/API/inventar/getUsers.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setUsers(data);
              });
          };        
          useEffect(() => {
            fetchDataUsers();
          }, []);

          const people = [];
          users.map((post) => (                        
            people.push(post.login)          
          ))
                         
          const handleChange = (event) => {
          const { name, value } = event.target;
        setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
       };
  
      const handleSubmit = (event) => {
      event.preventDefault();      
      if (formData.login!=='' & formData.passwd!=='') {      
      let loginfound = people.includes(formData.login);      
      let passwdfound = people.includes(formData.passwd);      
      if (loginfound === passwdfound && formData.login === formData.passwd && loginfound === true) {
      localStorage.setItem("fio", formData.login);  
      setIsModalOpen(false); 
      setTimeout(window.location.reload(), 2000);     
      }
      else
      {
        enqueueSnackbar({
          variant: "error",
          message: "Логин или пароль введены неверно!",
        });
      }
      }
    else
    enqueueSnackbar({
      variant: "error",
      message: "Введите логин и пароль!",
    });
  };

  const handleRegSubmit = (event) => {
    event.preventDefault();    
    var params = new URLSearchParams();
    params.set('login', login);
    params.set('passwd', passwd);
    params.set('fio', fio);
    params.set('post', post);
    let loginfound = people.includes(login);
    if (loginfound === true) {
      enqueueSnackbar({
        variant: "error",
        message: "Логин уже зарегистрирован. Введите другой логин!",
      }); 
    }   
    if (login !== "" && passwd !== "" && fio !=="" && post !=="" && loginfound === false) { 
    fetch('https://seric0.kz/API/inventar/addUser.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setIsModal2Open(false);
        setIsModalOpen(true);
        setTimeout(window.location.reload(), 2000);                       
      }           
   ); 
  }
  else {    
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    });    
  }
  }

return (
  <homestatic>
      {loading && <LoadingScreen />}         
      <main className={homestatic.main}>      
      {isModalOpen && (
        <div className={homestatic.modal}>
          <div className={homestatic.modal_content}>            
            <h2 className={homestatic.modal_maintext}>Авторизация</h2>            
            <form className={homestatic.modal_form} onSubmit={handleSubmit}>              
              <label htmlFor="login">Введите логин:</label>
              <input type="text" name="login" id="login" value={formData.login} onChange={handleChange} className={homestatic.modal_form_input} />
              <label htmlFor="passwd">Введите пароль:</label>
              <input type="password" name="passwd" id="passwd" value={formData.passwd} onChange={handleChange} className={homestatic.modal_form_input} />
            <button type="submit" className={homestatic.model_form_btn_auth}>Войти</button>            
            <button onClick={regModal} className={homestatic.model_form_btn_reg}>Зарегистрироваться</button>            
            </form>            
          </div>
        </div>
      )}
      {isModal2Open && (
      <div className={homestatic.modal}>
          <div className={homestatic.modal_content}>            
            <h2 className={homestatic.modal_maintext}>Регистрация</h2>            
            <form className={homestatic.modal_form}>              
              <label htmlFor="login">Введите логин:</label>
              <input type="text" name="login" id="login" value={login} onChange={event => setLogin(event.target.value)} className={homestatic.modal_form_input} />
              <label htmlFor="passwd">Введите пароль:</label>
              <input type="password" name="passwd" id="passwd" value={passwd} onChange={event => setPasswd(event.target.value)} className={homestatic.modal_form_input} />
              <label htmlFor="fio">Введите ФИО:</label>
              <input type="text" name="fio" id="fio" value={fio} onChange={event => setFio(event.target.value)} className={homestatic.modal_form_input} />
              <label htmlFor="post">Введите должность:</label>
              <input type="text" name="post" id="post" value={post} onChange={event => setPost(event.target.value)} className={homestatic.modal_form_input} />
            <button className={homestatic.model_form_btn_auth} onClick={handleRegSubmit}>Регистрация</button>            
            <button onClick={authModal} className={homestatic.model_form_btn_reg}>Войти</button>            
            </form>            
          </div>
        </div>
         )} 
      <h1 className={homestatic.mainstatic}>Общая статистика (Количество)</h1>
      {comps.map((post) => (              
      <p className={homestatic.text}>{post.name_inv} - {post.count} штук</p>
      ))}           
      </main>
  </homestatic>
);
}

export default HomeStatic;
