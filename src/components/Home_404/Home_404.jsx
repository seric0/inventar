import React from "react";

import home_404 from "./Home_404.module.scss"

function Home_404() {        
    return (
        <home_404>
            <main className={home_404.main}>
            <h1 className={home_404.mainstatic}>Такой страницы не существует!</h1>                        
            </main>
        </home_404>
    );
}

export default Home_404;
