import React, { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import TabPanel from '@mui/lab/TabPanel';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useSnackbar } from "notistack";
import { FaEdit } from "react-icons/fa";
import { FaAddressBook } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaFileExcel } from "react-icons/fa";
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import * as XLSX from 'xlsx';
import cabinetsStore from "../../store/store";

import allnetwork from "./NetworkEquipment.module.scss"

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,  
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  padding: '33px',
};
function NetworkEquipment() {        

        var indexone = 1;        
        const [posts, setPosts] = useState([]);
        const [currentPost, setCurrentPost] = useState(null);
        const [filteredPosts, setFilteredPosts] = useState([]);
        const [page, setPage] = useState(1);
        const [searchtext, setSearchText] = useState('');
        const [searchparam, setSearchParam] = useState('2');
        
        const fetchData = () => {            
            fetch(`http://seric0.kz/API/inventar/getNetwork.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setPosts(data);
              });
          };        
          useEffect(() => {
            fetchData();
          }, []);

        const [spr, setSpr] = useState([]);   
        const fetchDataSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getSprMainEquipment.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSpr(data);
            });
        };        
        useEffect(() => {
          fetchDataSpr();
        }, []);

        // const [cabinet, setCabinet] = useState([]);   
        //         const fetchDataCab = () => {            
        //           fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
        //             .then((response) => {
        //               return response.json();
        //             })
        //             .then((data) => {
        //               setCabinet(data);
        //             });
        //         };        
        //         useEffect(() => {
        //           fetchDataCab();
        //         }, []);

        useEffect(() => {          
          cabinetsStore.fetchPosts();
        }, []);
                
    const [open, setOpen] = React.useState(false);
    const [openjournal, setOpenJournal] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const handleCloseJourlan = () => setOpenJournal(false);

    const [name, setName] = useState('');
    const [col, setCol] = useState('');
    const [inv, setInv] = useState('');
    const [serial, setSerial] = useState('');
    const [cab, setCab] = useState('');
    const [inv_count, setCount] = useState('');
    const [inv_expenditure, setExpenditure] = useState('');
    const [invtype, setInvType] = useState('');
    const [property, setProperty] = useState('');
    const [work, setWork] = useState('');
    const [status, setStatus] = useState('');

    const { enqueueSnackbar } = useSnackbar();

    const saveData = () => { 
      const currentDate = new Date();
      const date_equipment = currentDate.toISOString().slice(0, 19).replace('T', ' '); 

      const updatedData = {
        id: currentPost.id,  
        name_equipment: name,
        property: property,
        inv_number: inv,
        serial_number: serial,
        cabinet: cab,
        inv_count: inv_count,
        inv_expenditure: inv_expenditure,
        inventar_type: invtype,
        work: work,
        status: status,
        date_equipment: date_equipment   
      };

      if (parseInt(inv_count) > 0 && parseInt(inv_expenditure) >= 0 && parseInt(inv_count) >= parseInt(inv_expenditure)) {       
      fetch(`https://seric0.kz/API/inventar/updateEquipment.php`, {
        method: 'POST',
        body: JSON.stringify(updatedData),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },    
      })
        .then((response) => response.json())
        .then(() => {                        
            enqueueSnackbar({
              variant: "success",
              message: "Данные обновлены",
            });
          //setTimeout(window.location.reload(), 4000);          
          fetchData();          
          handleClose();        
        })
        .catch((error) => {
          enqueueSnackbar({
            variant: "error",
            message: "Ошибка при обновлении "+error,
          })
        });
      }
      else {
        enqueueSnackbar({
          variant: "error",
          message: "Расход больше количества или количество равно 0",
        })
      }   
    };

    const saveDataJournal = () => {
    }

    const openData = (post) => {
      setCurrentPost(post);
      setName(post.name_equipment);
      setProperty(post.property);                
      setInv(post.inv_number);
      setSerial(post.serial_number);
      setCab(post.cabinet);
      setCount(post.inv_count);
      setExpenditure(post.inv_expenditure);
      setInvType(post.inventar_type);
      setWork(post.work);
      setStatus(post.status);
      setOpen(true);
    }

    const openJournal = (id) => { 
    }

    const exportToExcel = () => {
                      var excelindex = 1;
                      const headers = ['№ п/п', 'Наименование', 'Свойство', 'Инвентарный номер', '№ кабинета', 'Количество', 'Расход', 'Статус работоспособности', 'Причина'];                      
                      const data = posts.map(item => [excelindex++, item.name_equipment, item.property,item.inv_number, item.name,item.inv_count, item.inv_expenditure, item.work, item.status]);                        
                      const ws = XLSX.utils.aoa_to_sheet([headers, ...data]);          
                      const wb = XLSX.utils.book_new();
                      XLSX.utils.book_append_sheet(wb, ws, 'Запасные части');          
                      XLSX.writeFile(wb, 'data.xlsx');
                    };

    const [postPerPage] = useState(10);  
    const lastPost = page * postPerPage;
    const firstPost = lastPost - postPerPage;
    const allcurrentPost = filteredPosts.slice(firstPost, lastPost);            
    const PageCount = Math.ceil(filteredPosts.length / postPerPage);            
    const ChangePage = ( event, value ) => {                                        
      setPage(value);          
    };
    
    const findData = () => {                                                                                                                       
      if (searchtext) {
        let filtered = [];

        if (searchparam === '1') {
          filtered = posts.filter((post) => post.name_equipment.toLowerCase().includes(searchtext.toLowerCase()));
        }
        
        if (searchparam === '2') {
          filtered = posts.filter((post) =>                          
            post.inv_number && post.inv_number.includes(searchtext));
        }
        
                      
        if (searchparam === '3') {
          filtered = posts.filter((post) => post.name.includes(searchtext));
        }
        
        setFilteredPosts(filtered);
      } else {
        setFilteredPosts(posts);
      }
       }
                
      useEffect(() => {
      findData();
      }, [posts]);

    return (                                        
        <div className={allnetwork.main}>                                 
        <TabPanel value="2" sx={{ width: '1024px' }}>
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование/инвентарный или серийный номер" className={allnetwork.searchinput} value={searchtext} onChange={event => setSearchText(event.target.value)}/>
        </label>
        <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={searchparam}
                  label="Type"
                  sx={{ marginLeft: '15px' }}
                  onChange={event => setSearchParam(event.target.value)}
                >                  
                    <MenuItem value="1">Наименование</MenuItem>
                    <MenuItem value="2">Инвентарный номер</MenuItem>                    
                    <MenuItem value="3">№ кабинета</MenuItem>                                        
        </Select>
        <Button variant="contained" title="Экспорт в excel" onClick={exportToExcel} sx={{ marginLeft: '20px', height: '30px'}}><FaFileExcel /></Button>
        <Button variant="contained" title="Поиск" onClick={findData} sx={{ marginLeft: '20px', height: '30px' }}><FaSearch /></Button>
        <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
                      <form id="form" className={allnetwork.input_forms}>
                            <label>Наименование</label>                            
                            <input type="text" value={name} className={allnetwork.textinput} name="name" id="name" required minlength="4" onChange={event => setName(event.target.value)}/>                            
                            <label>Свойство:</label>                            
                            <input type="text" value={property} className={allnetwork.textinput} name="property" id="property" required minlength="6" onChange={event => setProperty(event.target.value)}/>                            
                            <label>Инвентарный номер</label>                                                                       
                            <input type="text" value={inv} className={allnetwork.textinput} name="inv_number" id="inv_number" required minlength="5" onChange={event => setInv(event.target.value)}/>                                           
                            <label>Серийный номер</label>             
                            <input type="text" value={serial} className={allnetwork.textinput} name="serial_number" id="serial_number" required minlength="5" onChange={event => setSerial(event.target.value)}/>                                                                                
                            <label>№ кабинета</label>
                            <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >
                  {cabinetsStore.cabinets.map((post) => (
                    <MenuItem key={post.id} value={post.id} sx={{ '&:hover': { backgroundColor: 'rgba(0, 0, 0, 0.12)' } }}>{post.name}</MenuItem>
                  ))}
                </Select>
                            <label>Количество:</label>
                            <input type="text" value={inv_count} className={allnetwork.textinput} name="inv_count" id="inv_count" required onChange={event => setCount(event.target.value)}/>                                         
                            <label>Расход:</label>
                            <input type="text" value={inv_expenditure} className={allnetwork.textinput} name="inv_expenditure" id="inv_expenditure" required onChange={event => setExpenditure(event.target.value)}/>                                         
                            <label>Тип:</label>
                            <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={invtype}
                  label="Type"
                  onChange={event => setInvType(event.target.value)}
                >
                  {spr.map((post) => (
                  <MenuItem key={post.id} value={post.id} sx={{ '&:hover': {backgroundColor: 'rgba(0, 0, 0, 0.12)'}, "&$selected": {backgroundColor: "red"} }}>{post.name_inv}</MenuItem>
                  ))}
                </Select>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={work}
                  label="Type"
                  onChange={event => setWork(event.target.value)}
                >
                  <MenuItem value="Работает">Работает</MenuItem>
                  <MenuItem value="Не работает">Не работает</MenuItem>
                </Select>
                <label>Причина:</label>
                            <textarea value={status} className={allnetwork.textinput} name="status" id="status" required onChange={event => setStatus(event.target.value)}/>                                                                                       
                            <Button variant="contained" onClick={saveData}>Сохранить</Button>
                        </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={openjournal}
                    onClose={handleCloseJourlan}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleCloseJourlan} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
                    <form id="form" className={allnetwork.input_forms}>
                            <label>Количество:</label>                            
                            <input type="text" Value={col} className={allnetwork.textinput} name="col" id="col" required minlength="4" onChange={event => setCol(event.target.value)}/>                                                                                  
                <InputLabel id="demo-simple-select-label">№ кабинета</InputLabel>                
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >                  
                {cabinetsStore.cabinets.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name}</MenuItem>
                  ))}
                </Select>                                             
                            <Button variant="contained" onClick={saveDataJournal}>Сохранить</Button>
                </form>                        
                    </Box>
                </Modal>
                <Stack spacing={2}>
        {posts.length > 0 && (        
        <table id="info-table">
          <thead>
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>
                <th>Свойство</th>
                <th>Инвентарный номер</th>
                <th>Серийный номер</th>
                <th>№ кабинета</th>
                <th>Количество</th>
                <th>Расход</th>
                <th>Статус работоспособности</th>
                <th>Причина</th>
            </tr>
            </thead>
            <tbody id="tbody">
            {allcurrentPost.map((post) => (            
            <tr id="tr" key={post.id}>                               
                <td>{indexone++}</td>
                <td>{post.name_equipment || 'Не указано'}</td>
                <td>{post.property || 'Не указано'}</td>                
                <td>{post.inv_number || 'Не указано'}</td>
                <td>{post.serial_number || 'Не указано'}</td>
                <td>{post.name || 'Не указано'}</td>
                <td>{post.inv_count || 'Не указано'}</td>
                <td>{post.inv_expenditure || 'Не указано'}</td>
                <td>{post.work || 'Не указано'}</td>
                <td>{post.status || 'Не указано'}</td>
                <td><Button variant="contained" title="Редактировать" onClick={() =>{openData(post)}}><FaEdit /></Button></td>
                <td><Button variant="contained" title="Журнал" onClick={() =>{openJournal(post.id)}}><FaAddressBook /></Button></td>
            </tr>            
            ))}
            </tbody>                      
        </table>        
        )}
        </Stack>
        <Pagination
        count={PageCount}        
        showFirstButton
        showLastButton               
        onChange={ChangePage}                 
        disableInitialCallback={true}        
        initialPage={1}
        color="primary"
        size="large"        
      />   
      </TabPanel>
    </div>        
    );
}

export default NetworkEquipment;
