import React, { useState, useEffect } from "react";
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Button from '@mui/material/Button';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import Modal from '@mui/material/Modal';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { useSnackbar } from "notistack";
import { FaPlus } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaSave } from "react-icons/fa";
import AllEquipments from "../../components/AllEquipments/AllEquipments";
import NetworkEquipment from "../../components/NetworkEquipment/NetworkEquipment";
import CartrigesEquipment from "../../components/CartrigesEquipment/CartrigesEquipment";
import KeyaboardMouseEquipment from "../../components/KeyaboardMouseEquipment/KeyaboardMouseEquipment";
import Cable from "../../components/Cable/Cable";

import maininventar from "./MainEquipment.module.scss"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

function MainEquipment() {          
    const [value, setValue] = useState('1');
                                  
    const handleChange = (event, newValue) => {
    setValue(newValue);}
                                                                                
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);    
    
    const [name, setName] = useState('');
    const [inv, setInv] = useState('');
    const [serial, setSerial] = useState('');
    const [cab, setCab] = useState('');
    const [inv_count, setCount] = useState('');
    const [inv_expenditure, setExpenditure] = useState('');
    const [invtype, setInvType] = useState('');
    const [property, setProperty] = useState('');
    const [work, setWork] = useState('');
    const [status, setStatus] = useState('');

const { enqueueSnackbar } = useSnackbar();

// const [posts, setPosts] = useState([]);        
//         const fetchData = () => {            
//             fetch(`http://seric0.kz/API/inventar/getEquipment.php`)
//               .then((response) => {
//                 return response.json();
//               })
//               .then((data) => {
//                 setPosts(data);
//               });
//           };        
//           useEffect(() => {
//             fetchData();
//           }, []);

const [spr, setSpr] = useState([]);   
        const fetchDataSpr = () => {            
          fetch(`http://seric0.kz/API/inventar/getSprMainEquipment.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSpr(data);
            });
        };        
        useEffect(() => {
          fetchDataSpr();
        }, []);

        const [cabinet, setCabinet] = useState([]);   
        const fetchDataCab = () => {            
          fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setCabinet(data);
            });
        };        
        useEffect(() => {
          fetchDataCab();
        }, []);

const saveData = () => {
  const currentDate = new Date();  
  const date_equipment = currentDate.toISOString().slice(0, 19).replace('T', ' ');     
  var params = new URLSearchParams();
  params.set('name_equipment', name);
  params.set('property', property);
  params.set('inv_number', inv);
  params.set('serial_number', serial);
  params.set('cabinet', cab);
  params.set('inv_count', inv_count);
  params.set('inv_expenditure', inv_expenditure);
  params.set('inventar_type', invtype);  
  params.set('work', work); 
  params.set('status', status);
  params.set('date_equipment', date_equipment);  
  if (name !== "" && cab !=="" && inv_count !=="" && invtype !== "" && (parseInt(inv_count) > 0 && parseInt(inv_expenditure) >= 0 && parseInt(inv_count) >= parseInt(inv_expenditure))) { 
  fetch('https://seric0.kz/API/inventar/addEquipment.php', {
    method: 'POST',
    body: params
 }).then(
    response => {
       return response.text();              
    }  
 ).then(
    text => {                  
      enqueueSnackbar({
        variant: "success",
        message: "Данные добавлены",
      });
      //fetchData();
      setTimeout(window.location.reload(), 2000);
      handleClose();       
    }           
 ); 
}
else {
  enqueueSnackbar({
    variant: "error",
    message: "Введите все данные!",
  }); 
}
}

// const tableSearch = () => {
//   var phrase = document.getElementById('search-text');
//   var table = document.getElementById('info-table');
//   var regPhrase = new RegExp(phrase.value, 'i');
//   var flag = false;
//   for (var i = 1; i < table.rows.length; i++) {
//       flag = false;
//       for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
//           flag = regPhrase.test(table.rows[i].cells[j].innerHTML);          
//           if (flag) break;
//       }
//       if (flag) {
//           table.rows[i].style.display = "";
//       } else {
//           table.rows[i].style.display = "none";
//       }      
//   }  
// }

    return (                                        
        <div className={maininventar.main}>            
            <h1 className={maininventar.mainstatic}>Инвентаризация (запасные части)</h1>
            <form>                                
                <Button variant="contained" onClick={handleOpen} sx={{ height: '30px'}}><FaPlus /></Button>
                {/* <Button variant="contained" onClick={tableSearch} sx={{ marginLeft: '20px', height: '30px'}}><FaSearch /></Button> */}
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
                      <form id="form" className={maininventar.input_forms}>
                            <label>Наименование:</label>                            
                            <input type="text" value={name} className={maininventar.textinput} name="name_equipment" id="name_equipment" required minlength="4" onChange={event => setName(event.target.value)}/>                            
                            <label>Свойство:</label>                            
                            <input type="text" value={property} className={maininventar.textinput} name="property" id="property" required minlength="6" onChange={event => setProperty(event.target.value)}/>                            
                            <label>Инвентарный номер:</label>                                                                       
                            <input type="text" value={inv} className={maininventar.textinput} name="inv_number" id="inv_number" required minlength="5" onChange={event => setInv(event.target.value)}/>                                           
                            <label>Серийный номер:</label>             
                            <input type="text" value={serial} className={maininventar.textinput} name="serial_number" id="serial_number" required minlength="5" onChange={event => setSerial(event.target.value)}/>                                                    
                            <label>№ кабинета:</label>
                            <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >
                  {cabinet.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name}</MenuItem>
                  ))}
                </Select>
                            <label>Количество:</label>
                            <input type="text" value={inv_count} className={maininventar.textinput} name="inv_count" id="inv_count" required onChange={event => setCount(event.target.value)}/>                                         
                            <label>Расход:</label>
                            <input type="text" value={inv_expenditure} className={maininventar.textinput} name="inv_expenditure" id="inv_expenditure" required onChange={event => setExpenditure(event.target.value)}/>                                         
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={invtype}
                  label="Type"
                  onChange={event => setInvType(event.target.value)}
                >
                  {spr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                  ))}
                </Select>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={work}
                  label="Type"
                  onChange={event => setWork(event.target.value)}
                >
                  <MenuItem value="Работает">Работает</MenuItem>
                  <MenuItem value="Не работает">Не работает</MenuItem>
                </Select>
                <label>Причина:</label>
                            <textarea value={status} className={maininventar.textinput} name="status" id="status" required onChange={event => setStatus(event.target.value)}/>                           
                            <Button variant="contained" onClick={saveData}><FaSave /></Button>
                        </form>                        
                    </Box>
                </Modal>                
            </form>
            <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange}>
          <Tab label="Запасные части" value="1" />
            <Tab label="Сетевое оборудование" value="2" />
            <Tab label="Картриджы" value="3" />
            <Tab label="Клавиатура и мышь" value="4" />            
            <Tab label="Кабеля" value="5" />
          </TabList>
        </Box>
        <AllEquipments/>
        <NetworkEquipment/>
        <CartrigesEquipment/>
        <KeyaboardMouseEquipment/>
        <Cable/>
      </TabContext>
    </Box>
    </div>        
    );
}

export default MainEquipment;
