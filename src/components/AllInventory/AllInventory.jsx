import React, { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import TabPanel from '@mui/lab/TabPanel';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useSnackbar } from "notistack";
import { FaEdit } from "react-icons/fa";
import { FaFolderPlus } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaSave } from "react-icons/fa";
import { FaDesktop } from "react-icons/fa";
import { FaFileExcel } from "react-icons/fa";
import LoadingScreen from "../Loader/Loader";
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import * as XLSX from 'xlsx';
import inventoryStore from "../../store/store";

import allinventar from "./AllInventory.module.scss"

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  padding: '33px',
};

function AllInventory() {        
         
         const [search, setSearch] = useState('');                                           
         const [page, setPage] = useState(1);
         const [filteredPosts, setFilteredPosts] = useState([]); 
         const [searchtext, setSearchText] = useState('');
         const [searchparam, setSearchParam] = useState('2');
         const [currentPostInv, setCurrentPostInv] = useState(null);

        var indexone = 1;  
        const [loading, setLoading] = useState(true);        
        useEffect(() => {
        setTimeout(() => setLoading(false), 2000);
        }, [])  
        
        const [posts, setPosts] = useState([]);        
        const fetchData = () => {            
            fetch(`http://seric0.kz/API/inventar/getInventar.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setPosts(data);
              });
          };        
          useEffect(() => {
            fetchData();
          }, []);
         
          const [open, setOpen] = useState(false);                    
          const [postone, setPostOne] = useState([]);                              
          const fetchOneData = (id) => {            
              fetch(`http://seric0.kz/API/inventar/getInventarOne.php?id=${id}`)
                .then((response) => {
                  if (id!==undefined)
                  setOpen(true);                  
                  return response.json();
                })
                .then((data) => {                                    
                  setPostOne(data);                                                      
                });
            };        
            useEffect(() => {
              fetchOneData();              
            }, []);

            // const [names, setNames] = useState([]);   
            // const fetchDataSprName = () => {            
            //   fetch(`http://seric0.kz/API/inventar/getSprNames.php`)
            //     .then((response) => {
            //       return response.json();
            //     })
            //     .then((data) => {
            //       setNames(data);
            //     });
            // };        
            // useEffect(() => {
            //   fetchDataSprName();
            // }, []);

            useEffect(() => {          
              inventoryStore.fetchNames();
            }, []);
                              
        
        const [spr, setSpr] = useState([]);   
        const fetchDataSpr = () => {            
          fetch(`http://seric0.kz/API/inventar/getSprMain.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSpr(data);
            });
        };        
        useEffect(() => {
          fetchDataSpr();
        }, []);

        const [spr_manufacturer, setSprManufacturer] = useState([]);
          const fetchDataSprManufacturer = () => {
            fetch(`https://seric0.kz/API/inventar/getSprManufacturer.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setSprManufacturer(data);
              });
          };
          useEffect(() => {
            fetchDataSprManufacturer();
          }, []);

          // const [cabinet, setCabinet] = useState([]);   
          //         const fetchDataCab = () => {            
          //           fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
          //             .then((response) => {
          //               return response.json();
          //             })
          //             .then((data) => {
          //               setCabinet(data);
          //             });
          //         };        
          //         useEffect(() => {
          //           fetchDataCab();
          //         }, []);

          useEffect(() => {          
            inventoryStore.fetchPosts();
          }, []);
                       
    const handleClose = () => setOpen(false);
    const [opencomp, setOpenComp] = useState(false);
    const [openInv, setOpenInv] = useState(false); 
    const handleCloseInv = () => setOpenInv(false);
    const [selectedId, setSelectedId] = useState(null);
    const [name, setName] = useState('');
    const [manufacturer_id, setManufacturer] = useState('');
    const [inv, setInv] = useState('');
    const [serial, setSerial] = useState('');
    const [cab, setCab] = useState('');    
    const [invtype, setInvType] = useState('');
    const [model, setModel] = useState('');
    const [work, setWork] = useState('');
    const [status, setStatus] = useState('');

    const { enqueueSnackbar } = useSnackbar();

    const saveData = () => { 
      const currentDate = new Date();
      const date_inv = currentDate.toISOString().slice(0, 19).replace('T', ' '); 

      const updatedData = {
        id: currentPostInv.id,  
        name_inventar_id: name,        
        model: model,
        inv_number: inv,
        serial_number: serial,
        cabinet: cab,
        manufacturer_id: manufacturer_id,
        inventar_type: invtype,
        work: work,
        status: status,
        date_inv: date_inv   
      };
             
      fetch(`https://seric0.kz/API/inventar/updateInventar.php`, {
        method: 'POST',
        body: JSON.stringify(updatedData),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },    
      })
        .then((response) => response.json())
        .then(() => {                        
            enqueueSnackbar({
              variant: "success",
              message: "Данные обновлены",
            });
          //setTimeout(window.location.reload(), 4000);          
          fetchData();          
          handleCloseInv();        
        })
        .catch((error) => {
          enqueueSnackbar({
            variant: "error",
            message: "Ошибка при обновлении "+error,
          })
        });      
    };

    const [spectone, setSpecOne] = useState([]);
                                const fetchSpecData = (id) => {
                                  if (id !== undefined) {              
                                    fetch(`https://seric0.kz/API/inventar/getSpecOne1.php?idcomp=${id}`)
                                      .then((response) => {
                                        return response.json();
                                      })
                                      .then((data) => {
                                        setSpecOne(data);                  
                                      });
                                  }
                                };
                                useEffect(() => {
                                  fetchSpecData();
                                }, []);

    const openComp = (id) => {                              
      fetchSpecData(id);           
      setOpenComp(true);          
    }

    const openData = (id) => {
      setSelectedId(id);          
      setOpen(true);          
    }

    const exportToExcel = () => {
              var excelindex = 1;
              const headers = ['№ п/п', 'Наименование', 'Модель', 'Инвентарный номер', 'Серийный номер', '№ кабинета', 'Статус работоспособности', 'Причина'];                      
              const data = posts.map(item => [excelindex++, item.name_inv, item.name_spr+' '+item.model,item.inv_number, item.serial_number,item.name, item.work, item.status]);                        
              const ws = XLSX.utils.aoa_to_sheet([headers, ...data]);          
              const wb = XLSX.utils.book_new();
              XLSX.utils.book_append_sheet(wb, ws, 'Копьютеры');          
              XLSX.writeFile(wb, 'data.xlsx');
            };

            const [postPerPage] = useState(10);  
            const lastPost = page * postPerPage;
            const firstPost = lastPost - postPerPage;
            const currentPost = filteredPosts.slice(firstPost, lastPost);
            const PageCount = Math.ceil(filteredPosts.length / postPerPage);
            const ChangePage = ( event, value ) => {                                        
              setPage(value);          
            };

    const findData = () => {                                                                              
                      //   if (searchtext) {                                          
                      //   if (searchparam == 2) {
                      //     const filtered = posts.filter((post) =>                                                        
                      //       post.inv_number.includes(searchtext)                                                           
                      //   );                            
                      //     setFilteredPosts(filtered);
                      //   }
                      //   if (searchparam == 3) {
                      //     const filtered = posts.filter((post) =>                                                        
                      //       post.serial_number.includes(searchtext)                                                           
                      //   );                            
                      //     setFilteredPosts(filtered);
                      //   }
                      // }                                                                                                     
                      //   else {
                      //     setFilteredPosts(posts);
                      //   }   
                      
                      if (searchtext) {
                        let filtered = [];

                        if (searchparam === '1') {
                          filtered = posts.filter((post) => post.name_inv.toLowerCase().includes(searchtext.toLowerCase()));
                        }
                        
                        if (searchparam === '2') {
                          filtered = posts.filter((post) => post.inv_number.includes(searchtext));
                        }
                      
                        if (searchparam === '3') {
                          filtered = posts.filter((post) => post.serial_number.includes(searchtext));
                        }

                        if (searchparam === '4') {
                          filtered = posts.filter((post) => post.name.includes(searchtext));
                        }
                        
                        setFilteredPosts(filtered);
                      } else {
                        setFilteredPosts(posts);
                      }
                       }
                                
                      useEffect(() => {
                      findData();
                      }, [posts]);

                      const openInvData = (post) => { 
                        setCurrentPostInv(post);
                        setName(post.name_inventar_id);
                        setManufacturer(post.manufacturer_id);
                        setModel(post.model);                
                        setInv(post.inv_number);
                        setSerial(post.serial_number);
                        setCab(post.cabinet);                        
                        setInvType(post.inventar_type);
                        setWork(post.work);
                        setStatus(post.status);                        
                        setOpenInv(true);
                      }
                     
    return (                                        
        <div className={allinventar.main}>
        {loading && <LoadingScreen />}                                 
        <TabPanel value="1" sx={{ width: '1024px' }}>
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите инвентарный номер" className={allinventar.searchinput} value={searchtext} onChange={event => setSearchText(event.target.value)}/>
        </label>
        <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={searchparam}
                  label="Type"
                  sx={{ marginLeft: '15px' }}
                  onChange={event => setSearchParam(event.target.value)}
                >                  
                    <MenuItem value="1">Наименование</MenuItem>
                    <MenuItem value="2">Инвентарный номер</MenuItem>
                    <MenuItem value="3">Серийный номер</MenuItem>
                    <MenuItem value="4">№ кабинета</MenuItem>                                        
        </Select>
        <Button variant="contained" title="Экспорт в excel" onClick={exportToExcel} sx={{ marginLeft: '20px', height: '30px'}}><FaFileExcel /></Button>
        <Button variant="contained" title="Поиск" onClick={findData} sx={{ marginLeft: '20px', height: '30px' }}><FaSearch /></Button>
        <Modal
          open={openInv}
          onClose={handleCloseInv}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description">
          <Box sx={style}>
            <Button onClick={handleCloseInv} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
            <form id="form" className={allinventar.input_forms}>
              <label>Наименование:</label>              
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={name}
                label="Type"
                onChange={event => setName(event.target.value)}
              >
                {inventoryStore.sprnames.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <InputLabel id="demo-simple-select-label">Производитель:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={manufacturer_id}
                label="Type"
                onChange={event => setManufacturer(event.target.value)}
              >
                {spr_manufacturer.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr}</MenuItem>
                ))}
              </Select>
              <label>Модель:</label>
              <input type="text" value={model} className={allinventar.textinput} name="model" id="model" required onChange={event => setModel(event.target.value)} />
              <label>Инвентарный номер:</label>             
              <input type="text" value={inv} className={allinventar.textinput} name="inv_number" id="inv_number" required onChange={event => setInv(event.target.value)} />              
              <label>Серийный номер:</label>
              <input type="text" value={serial} className={allinventar.textinput} name="serial_number" id="serial_number" required onChange={event => setSerial(event.target.value)} />
              <label>№ кабинета:</label>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >
                  {inventoryStore.cabinets.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name}</MenuItem>
                  ))}
                </Select>
              <InputLabel id="demo-simple-select-label">Тип:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={invtype}
                label="Type"
                onChange={event => setInvType(event.target.value)}
              >
                {spr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={work}
                  label="Type"
                  onChange={event => setWork(event.target.value)}
                >
                  <MenuItem value="Работает">Работает</MenuItem>
                  <MenuItem value="Не работает">Не работает</MenuItem>
                </Select>
                <label>Причина:</label>
                            <textarea value={status} className={allinventar.textinput} name="status" id="status" required onChange={event => setStatus(event.target.value)}/>  
              <Button variant="contained" title="Сохранить" onClick={saveData} sx={{ height: '30px' }}><FaSave /></Button>
            </form>
          </Box>
        </Modal>         
        <Stack spacing={2}>           
        {posts.length > 0 && (        
        <table id="info-table">
          <thead>
            <tr>
                <th className={allinventar.info_nomer}>№ п/п</th>
                <th className={allinventar.info_name}>Наименование</th>                                
                <th>Модель</th>
                <th>Инвентарный номер</th>
                <th>Серийный номер</th>
                <th>№ кабинета</th>
                <th>Статус работоспособности</th>
                <th>Причина</th>
            </tr>
          </thead>                      
            <tbody id="tbody">                                    
            {currentPost.map((post) => (            
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td>{post.name_inv || 'Не указано'}</td>                                                
                <td>{post.name_spr+' '+post.model || 'Не указано'}</td>
                <td>{post.inv_number || 'Не указано'}</td>
                <td>{post.serial_number || 'Не указано'}</td>
                <td>{post.name || 'Не указано'}</td>
                <td>{post.work || 'Не указано'}</td>
                <td>{post.status || 'Не указано'}</td>
                <td><Button variant="contained" title="Редактировать" onClick={() =>{openInvData(post)}}><FaEdit /></Button></td>
                <td><Button variant="contained" title="Добавить характеристики" onClick={() => openData(post.id)}><FaFolderPlus /></Button></td>
                <td><Button variant="contained" title="Карточка основного средства" onClick={() => openComp(post.id)}><FaDesktop /></Button></td>                
            </tr>            
            ))}
            </tbody>                      
        </table>        
        )}
        </Stack>
        <Pagination
        count={PageCount}        
        showFirstButton
        showLastButton               
        onChange={ChangePage}                 
        disableInitialCallback={true}        
        initialPage={1}
        color="primary"
        size="large"        
      />  
      </TabPanel>
    </div>        
    );
}

export default AllInventory;
