import React, { useState, useEffect } from "react";
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import Modal from '@mui/material/Modal';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { useSnackbar } from "notistack";
import AllInventory from "../../components/AllInventory/AllInventory";
import CompsInventory from "../../components/CompsInventory/CompsInventory";
import SystemsInventory from "../../components/SystemsInventory/SystemsInventory";
import NoutsInventory from "../../components/NoutsInventory/NoutsInventory";
import PrintersInventory from "../../components/PrintersInventory/PrintersInventory";
import MonitorsInventory from "../../components/MonitorsInventory/MonitorsInventory";
import { FaPlus } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaSave } from "react-icons/fa";

import maininventar from "./MainInventory.module.scss"

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  padding: '33px',
};

function MainInventory() {
  const [value, setValue] = useState('1');
  
  const handleChange = (event, newValue) => {
    setValue(newValue);
  }

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);    

  const [name, setName] = useState('');
  const [name_invetar_id, setName_invetar_id] = useState('');  
  const [model, setModel] = useState('');
  const [inv, setInv] = useState('');
  const [serial, setSerial] = useState('');
  const [cab, setCab] = useState('');
  const [invtype, setInvType] = useState('');
  const [manufacturer_id, setManufacturer_id] = useState('');
  const [work, setWork] = useState('');
  const [status, setStatus] = useState('');
  
  const { enqueueSnackbar } = useSnackbar();

  const [data, setData] = useState([]);
  const [search, setSearch] = useState('');

  const [names, setNames] = useState([]);
  const fetchDataSprName = () => {
    fetch(`http://seric0.kz/API/inventar/getSprNames.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setNames(data);
      });
  };
  useEffect(() => {
    fetchDataSprName();
  }, []);

  const [spr, setSpr] = useState([]);
  const fetchDataSpr = () => {
    fetch(`http://seric0.kz/API/inventar/getSprMain.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSpr(data);
      });
  };
  useEffect(() => {
    fetchDataSpr();
  }, []);

  const [spr_manufacturer, setSprManufacturer] = useState([]);
  const fetchDataSprManufacturer = () => {
    fetch(`https://seric0.kz/API/inventar/getSprManufacturer.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSprManufacturer(data);
      });
  };
  useEffect(() => {
    fetchDataSprManufacturer();
  }, []);

   const [cabinet, setCabinet] = useState([]);   
          const fetchCabinet = () => {            
            fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setCabinet(data);
              });
          };        
          useEffect(() => {
            fetchCabinet();
          }, []);

  const saveData = () => {
    const currentDate = new Date();  
    const date_inv = currentDate.toISOString().slice(0, 19).replace('T', ' ');     
    var params = new URLSearchParams();
    params.set('name_invetar_id', name_invetar_id);
    params.set('model', model);    
    params.set('inv_number', inv);
    params.set('serial_number', serial);
    params.set('cabinet', cab);
    params.set('inventar_type', invtype);
    params.set('manufacturer_id', manufacturer_id);
    params.set('work', work); 
    params.set('status', status); 
    params.set('date_inv', date_inv);
    if (name_invetar_id !== "" && model !== "" && inv !== "" && cab !== "" && invtype !== "") {
      fetch('https://seric0.kz/API/inventar/addInvetar.php', {
        method: 'POST',
        body: params
      }).then(
        response => {
          return response.text();
        }
      ).then(
        text => {
          enqueueSnackbar({
            variant: "success",
            message: "Данные добавлены",
          });
          setTimeout(() => {
            window.location.reload();
        }, 2000);
          handleClose();
        }
      );
    }
    else {
      enqueueSnackbar({
        variant: "error",
        message: "Введите все данные!",
      });
    }
  }
  // var phrase = document.getElementById('search-text');  
  //var table = document.getElementById('info-table');
  // var regPhrase = new RegExp(phrase.value, 'i');
  // var flag = false;
  // for (var i = 1; i < table.rows.length; i++) {
  //     flag = false;
  //     for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
  //         flag = regPhrase.test(table.rows[i].cells[j].innerHTML);          
  //         if (flag) break;
  //     }
  //     if (flag) {
  //         table.rows[i].style.display = "";          
  //     } else {
  //         table.rows[i].style.display = "none";
  //     }      
  // }

  const [posts, setPosts] = useState([]);

  const fetchData = () => {
    fetch(`http://seric0.kz/API/inventar/getInventar.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setPosts(data);
      });
  };
  useEffect(() => {
    fetchData();
  }, []);

  const [postone, setPostOne] = useState([]);
  const fetchOneData = (id) => {
    if (id !== undefined) {
      setOpen(true);
      fetch(`http://seric0.kz/API/inventar/getInventarOne.php?id=${id}`)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setPostOne(data);
        });
    }
  };
  useEffect(() => {
    fetchOneData();
  }, []);

  
  const fetchSearchData = (searchname) => {
    fetch(`http://seric0.kz/API/inventar/getInventarSearch.php?name=${searchname}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSearch(data);
      });
  };
  useEffect(() => {
    fetchSearchData();
  }, []);

  const [checked1, setChecked1] = useState(false);
  const [checked2, setChecked2] = useState(false);
  const [checked3, setChecked3] = useState(false);  
  const [checked4, setChecked4] = useState(false);  
  
  const noInv = () => {
    setChecked4(!checked4);
    document.getElementById('inv_number').value = "Не указано";
    setInv = "Не указано";
  }

  // const findData = () => {
  //   var poisk;
  //   var searchtext = document.getElementById('info_message');
  //   searchtext.style.display = 'none';
  //   var phrase = document.getElementById('search-text');
  //   if (checked1 === true) {
  //     //  poisk = posts.filter(v => v.name.toLowerCase() === phrase.value.toLowerCase());
  //      poisk = posts.filter(v => v.name.toLowerCase().includes(phrase.value.toLowerCase()));
  //   }    
  //   if (checked2 === true) {
  //     poisk = posts.filter(v => v.inv_number.toLowerCase() === phrase.value.toLowerCase());
  //   }
  //   if (checked3 === true) {
  //     poisk = posts.filter(v => v.cabinet.toLowerCase() === phrase.value.toLowerCase());
  //   }
  //   if (checked2 === false && checked1 === false && checked3 === false)
  //   {
  //     enqueueSnackbar('Выберите параметр для поиска!', {
  //       variant: 'success',        
  //       style: {
  //         backgroundColor: '#1565c0',
  //         color: 'white',
  //         fontWeight: 'bold'
  //       },
  //     });
      // searchtext = document.getElementById('info_message');
      // searchtext.style.display ='block';
      //poisk = '';
    //}
      // poisk = posts.filter(v => v.inv_number === phrase.value);    
      // const table = document.getElementById('tbody');
      // table.innerHTML = '';
      //table.innerHTML = '';
      //table.remove();        
      // poisk.map((find) => (
      //   tr = document.createElement('tr'),      
      //   td.innerHTML = `<td>${poisk.id}</td><td>${poisk.name}</td><td>${poisk.inv_number}</td><td>${poisk.serial_number}</td><td>${poisk.cabinet}</td>`,      
      //   table.appendChild(td)
      //   ))
      // for (let i = 0; i < poisk.length; i++) {
      //   let row = document.createElement('tr');
      //   row.innerHTML = `<td>${i + 1}</td><td>${poisk[i].name}</td><td>${poisk[i].name_manufact + ' ' + poisk[i].model}</td><td>${poisk[i].inv_number}</td><td>${poisk[i].serial_number}</td><td>${poisk[i].cabinet}</td><td><button class="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium css-sghohy-MuiButtonBase-root-MuiButton-root" tabindex="0" type="button" title="Редактировать"><svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 576 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg><span class="MuiTouchRipple-root css-8je8zh-MuiTouchRipple-root"></span></button></td>`;
      //   table.appendChild(row);
      //   row.querySelector('.MuiButtonBase-root').addEventListener('click', () => {
      //     fetchOneData(poisk[i].id);
      //   });
      // const [postPerPage] = useState(10);  
      //           const lastPost = page * postPerPage;
      //           const firstPost = lastPost - postPerPage;
      //           const currentPost = search.slice(firstPost, lastPost);
      //           const PageCount = Math.ceil(search.length / postPerPage);
      //           const ChangePage = ( event, value ) => {                                        
      //             setPage(value);   
     //}           
    //   var searchtext = document.getElementById('info_message');
    //   searchtext.style.display = 'block';
    // }
    //   var regPhrase = new RegExp(phrase.value, 'i');
    //   var flag = false;
    //   for (var i = 1; i < table.rows.length; i++) {
    //   flag = false;
    //   for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
    //   flag = regPhrase.test(table.rows[i].cells[j].innerHTML);          
    //   if (flag) break;
    //     }
    //   if (flag) {
    //         table.rows[i].style.display = "";          
    //   } else {
    //         table.rows[i].style.display = "none";
    //   }      
    // }
  //}

  // const [postone, setPostOne] = useState([]);                                          
  //         const fetchSearchData = () => {              
  //           if (phrase !== null) {                                        
  //             fetch(`http://seric0.kz/API/inventar/searchInventar.php?inv_number=${phrase.value}`)
  //               .then((response) => {                  
  //                 return response.json();
  //               })
  //               .then((data) => {                  
  //                 setPostOne(data);
  //                 console.log(data);                  
  //               });
  //           }};        
  //           useEffect(() => {
  //             fetchSearchData();                                          
  //              //if (phrase!== null) {
  //              //findData(phrase);
  //              //}

  //           }, []);

  //           const input = document.querySelector('#inv_number');           

  return (
    <div className={maininventar.main}>
      <h1 className={maininventar.mainstatic}>Инвентаризация (основные средства)</h1>
      <form>
        <Button variant="contained" title="Добавить" onClick={handleOpen} sx={{ height: '30px' }}><FaPlus /></Button>
        {/* <Button variant="contained" title="Поиск" onClick={findData} sx={{ marginLeft: '20px', height: '30px' }}><FaSearch /></Button>
        <Checkbox id="CheckBox1" checked={checked1} onChange={() => setChecked1(!checked1)}></Checkbox><label>По имени</label>
        <Checkbox id="CheckBox2" checked={checked2} onChange={() => setChecked2(!checked2)}></Checkbox><label>По инвен.номеру</label>
        <Checkbox id="CheckBox3" checked={checked3} onChange={() => setChecked3(!checked3)}></Checkbox><label>По № кабинета</label>
        <span id="info_message" className={maininventar.info_message_search}>Выберите параметр для поиска!</span> */}
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description">
          <Box sx={style}>
            <Button onClick={handleClose} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
            <form id="form" className={maininventar.input_forms}>
              <label>Наименование:</label>
              {/* <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} /> */}
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={name_invetar_id}
                label="Type"
                onChange={event => setName_invetar_id(event.target.value)}
              >
                {names.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <InputLabel id="demo-simple-select-label">Производитель:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={manufacturer_id}
                label="Type"
                onChange={event => setManufacturer_id(event.target.value)}
              >
                {spr_manufacturer.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr}</MenuItem>
                ))}
              </Select>
              <label>Модель:</label>
              <input type="text" value={model} className={maininventar.textinput} name="model" id="model" required onChange={event => setModel(event.target.value)} />
              <label>Инвентарный номер:<Checkbox id="CheckBox4" checked={checked4} onChange={() => noInv()}></Checkbox><label>Без номера</label> </label>             
              <input type="text" value={inv} className={maininventar.textinput} name="inv_number" id="inv_number" required onChange={event => setInv(event.target.value)} />              
              <label>Серийный номер:</label>
              <input type="text" value={serial} className={maininventar.textinput} name="serial_number" id="serial_number" required onChange={event => setSerial(event.target.value)} />
              <label>№ кабинета:</label>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >
                  {cabinet.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name}</MenuItem>
                  ))}
                </Select>
              <InputLabel id="demo-simple-select-label">Тип:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={invtype}
                label="Type"
                onChange={event => setInvType(event.target.value)}
              >
                {spr.map((post) => (
                  <MenuItem  key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={work}
                  label="Type"
                  onChange={event => setWork(event.target.value)}
                >
                  <MenuItem value="Работает">Работает</MenuItem>
                  <MenuItem value="Не работает">Не работает</MenuItem>
                </Select>
                <label>Причина:</label>
                            <textarea value={status} className={maininventar.textinput} name="status" id="status" required onChange={event => setStatus(event.target.value)}/>  
              <Button variant="contained" title="Сохранить" onClick={saveData} sx={{ height: '30px' }}><FaSave /></Button>
            </form>
          </Box>
        </Modal>
      </form>
      <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={handleChange}>
              <Tab label="Вся техника" value="1" />
              <Tab label="Компьютеры" value="2" />
              <Tab label="Системные блоки" value="3" />
              <Tab label="Ноутбуки" value="4" />
              <Tab label="МФУ/Принтеры" value="5" />
              <Tab label="Мониторы" value="6" />
            </TabList>
          </Box>
          <AllInventory />
          <CompsInventory />
          <SystemsInventory />
          <NoutsInventory />
          <PrintersInventory />
          <MonitorsInventory />
        </TabContext>
      </Box>
    </div>
  );
}

export default MainInventory;
