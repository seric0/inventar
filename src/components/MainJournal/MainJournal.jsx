import React, { useState, useEffect } from "react";
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Button from '@mui/material/Button';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import Modal from '@mui/material/Modal';
import TabPanel from '@mui/lab/TabPanel';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useSnackbar } from "notistack";
import { FaPlus } from "react-icons/fa";
import { FaPlusCircle } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaSave } from "react-icons/fa";
import LoadingScreen from "../Loader/Loader";

import maininventar from "./MainJournal.module.scss"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

function MainJournal() {              
    const [value, setValue] = useState('1');

    var indexone = 1;
    var indextwo = 1;
    var indexthree = 1;
    
    const [loading, setLoading] = useState(true);
        useEffect(() => {
        setTimeout(() => setLoading(false), 2000);
      }, [])           
    
    const handleChange = (event, newValue) => {
    setValue(newValue);}
                                                                                
    const [open, setOpen] = useState(false);
    const [open2, setOpen2] = useState(false);
    const [open3, setOpen3] = useState(false);
    const [open4, setOpen4] = useState(false);
    const [open5, setOpen5] = useState(false);
    const [open6, setOpen6] = useState(false);
    const [open7, setOpen7] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleOpen2 = () => setOpen2(true);
    const handleOpen3 = () => setOpen3(true);
    const handleOpen4 = () => setOpen4(true);
    const handleOpen5 = () => setOpen5(true);
    const handleOpen6 = () => setOpen6(true);
    const handleOpen7 = () => setOpen7(true);
    const handleClose = () => setOpen(false);
    const handleClose2 = () => setOpen2(false);
    const handleClose3 = () => setOpen3(false);
    const handleClose4 = () => setOpen4(false);
    const handleClose5 = () => setOpen5(false);
    const handleClose6 = () => setOpen6(false);
    const handleClose7 = () => setOpen7(false);    
    
    const [name, setName] = useState('');
    const [sprtype, setSprType] = useState('');    

const { enqueueSnackbar } = useSnackbar();

const [spr, setSpr] = useState([]);   
        const fetchDataSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getSprMain.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSpr(data);
            });
        };        
        useEffect(() => {
          fetchDataSpr();
        }, []);

        const [cabinetspr, setCabinetSpr] = useState([]);   
        const fetchCabinetSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setCabinetSpr(data);
            });
        };        
        useEffect(() => {
          fetchCabinetSpr();
        }, []);

        const [frequencyspr, setFrequencySpr] = useState([]);   
        const fetchFrequencySpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getFrequency.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setFrequencySpr(data);
            });
        };        
        useEffect(() => {
          fetchFrequencySpr();
        }, []);

        const [memoryspr, setMemorySpr] = useState([]);   
        const fetchMemorySpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getMemory.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setMemorySpr(data);
            });
        };        
        useEffect(() => {
          fetchMemorySpr();
        }, []);

        const [hddspr, setHddSpr] = useState([]);   
        const fetchHddSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getHdd.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setHddSpr(data);
            });
        };        
        useEffect(() => {
          fetchHddSpr();
        }, []);

        const [ocspr, setOcSpr] = useState([]);   
        const fetchOcSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getOC.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setOcSpr(data);
            });
        };        
        useEffect(() => {
          fetchOcSpr();
        }, []);

        const [manufacturer, setManufacturer] = useState([]);        
        const fetchSprData = () => {            
            fetch(`http://seric0.kz/API/inventar/getSprManufacturer.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setManufacturer(data);
              });
          };        
          useEffect(() => {
            fetchSprData();
          }, []);

const [sprEquipment, setSprEquipment] = useState([]);   
        const fetchDataSprEquipment = () => {            
          fetch(`https://seric0.kz/API/inventar/getJournal.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSprEquipment(data);
            });
        };        
        useEffect(() => {
          fetchDataSprEquipment();
        }, []);

  const saveData = () => {    
  var params = new URLSearchParams();
  const name_new = name.toLowerCase();
  const result = name_new.charAt(0).toUpperCase() + name_new.slice(1); 
  const passed1 = sprEquipment.some(n => n.name === result);
  const passed2 = spr.some(n => n.name === result);
  params.set('name_inv', name);
  params.set('spr_type', sprtype);
  if (passed1 === true || passed2 === true) {
    enqueueSnackbar({
      variant: "error",
      message: "Наименование уже есть!",
    });
  }
  else 
  {  
  if (name !== "" && sprtype !== "") {
  fetch('https://seric0.kz/API/inventar/addSpr.php', {
    method: 'POST',
    body: params
 }).then(
    response => {
       return response.text();       
    }  
 ).then(
    text => {                  
      enqueueSnackbar({
        variant: "success",
        message: "Данные добавлены",
      });
      setTimeout(window.location.reload(), 2000);
      handleClose();       
    }           
 ); 
}
else {
  enqueueSnackbar({
    variant: "error",
    message: "Введите все данные!",
  }); 
}
}
}

const saveDataFacture = () => {
  var params = new URLSearchParams();
  params.set('name_manufact', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addManufacture.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose2();       
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const saveDataFrequency = () => {
  var params = new URLSearchParams();
  params.set('name', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addFrequency.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose3();       
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const saveDataCab = () => {
  var params = new URLSearchParams();
  params.set('name', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addCabinet.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose4();
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const saveDataMemory = () => {
  var params = new URLSearchParams();
  params.set('name', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addMemory.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose5();       
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const saveDataHdd = () => {
  var params = new URLSearchParams();
  params.set('name', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addHdd.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose6();       
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const saveDataOC = () => {
  var params = new URLSearchParams();
  params.set('name', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addOC.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose7();       
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const tableSearch = () => {
  var phrase = document.getElementById('search-text');
  var table = document.getElementById('info-table');
  var regPhrase = new RegExp(phrase.value, 'i');
  var flag = false;
  for (var i = 1; i < table.rows.length; i++) {
      flag = false;
      for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
          flag = regPhrase.test(table.rows[i].cells[j].innerHTML);          
          if (flag) break;
      }
      if (flag) {
          table.rows[i].style.display = "";
      } else {
          table.rows[i].style.display = "none";
      }      
  }  
}

    return (                                        
        <div className={maininventar.main}>
          {loading && <LoadingScreen />}            
            <h1 className={maininventar.mainstatic}>Справочники</h1>                                            
                <Button variant="contained" onClick={handleOpen} sx={{ height: '30px'}}><FaPlus /></Button>
                <Button variant="contained" onClick={tableSearch} sx={{ marginLeft: '20px', height: '30px'}}><FaSearch /></Button>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name_inv" id="name_inv" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (типы основных средств)</MenuItem>
                    <MenuItem value="2">Справочник (типы запасных частей)</MenuItem>
                </Select>
                <Button variant="contained" onClick={saveData}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open2}
                    onClose={handleClose2}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose2} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name_manufact" id="name_manufact" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={saveDataFacture}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open3}
                    onClose={handleClose3}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose3} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={saveDataCab}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={open4}
                    onClose={handleClose4}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose4} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={saveDataFrequency}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={open5}
                    onClose={handleClose5}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose5} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={saveDataMemory}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={open6}
                    onClose={handleClose6}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose6} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Объем жесткого диска:</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={saveDataHdd}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={open7}
                    onClose={handleClose7}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose7} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={saveDataOC}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>                            
            <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange}>
          <Tab label="Журнал (основные средства)" value="1" sx={{ width: '230px' }}/>
          <Tab label="Журнал (запасные части)" value="2" sx={{ width: '230px' }}/>            
          <Tab label="Журнал (компьютеры со спецификациями)" value="3" sx={{ width: '230px' }}/>                              
          </TabList>
          <TabList onChange={handleChange}>
          <Tab label="Акты приема-передачи основных средств" value="4" sx={{ width: '230px' }}/>
          <Tab label="Акты списания основных средств" value="5" sx={{ width: '230px' }}/>
          <Tab label="Акты приема-передачи запасных частей" value="6" sx={{ width: '230px' }}/>                    
          </TabList>
        </Box>
        <TabPanel value="1">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>           
        {spr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>              
            </tr>                                    
            {spr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexone++}</td>
                <td id="allname_1">{post.name_inv}</td>                
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="2">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>           
        {sprEquipment.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>              
                <th>№ кабинета</th>
            </tr>                                    
            {sprEquipment.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indextwo++}</td>
                <td id="allname_2">{post.name_equipment}</td>     
                <td id="allname_2">{post.name}</td>
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="3">                
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>          
        <Button variant="contained" onClick={handleOpen2} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button> 
        {manufacturer.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>              
            </tr>                                    
            {manufacturer.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexthree++}</td>
                <td id="allname_2">{post.name_manufact}</td>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="4">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите номер кабинета" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen3} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>            
        {cabinetspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Номер кабинета</th>              
            </tr>                                    
            {cabinetspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td id="allname_2">{post.name}</td>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="5">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите тактовую частоту в цифрах" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen4} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {frequencyspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Тактовая частота</th>              
            </tr>                                    
            {frequencyspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td id="allname_2">{post.name} ГГц</td>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="6">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите количество в цифрах" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen5} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {memoryspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Количество оперативной памяти</th>              
            </tr>                                    
            {memoryspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td id="allname_2">{post.name > 100 ? post.name + " Мб" : post.name + " Гб"} </td>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="7">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите количество в цифрах" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen6} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {hddspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Объем жесткого диска</th>              
            </tr>                                    
            {hddspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td id="allname_2">{post.name > 100 ? post.name + " Гб" : post.name + " Тб"} </td>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="8">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen7} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {ocspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование операционной системы</th>              
            </tr>                                    
            {ocspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td id="allname_2">{post.name} </td>     
            </tr>            
            ))}                      
        </table>        
        )}
        </TabPanel>
      </TabContext>
    </Box>
    </div>        
    );
}

export default MainJournal;
