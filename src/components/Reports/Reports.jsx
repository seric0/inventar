import React, { useEffect, useState } from "react";
import LoadingScreen from "../Loader/Loader";

import reports from "./Reports.module.scss"

function Reports() {        
    const [comps, setComps] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchDataComp = () => {            
          fetch(`https://seric0.kz/API/inventar/getEquipment_count.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setComps(data);
            });
        };        
        useEffect(() => {
          fetchDataComp();
        }, []);

    return (                        
          <main className={reports.main}>            
          <h1 className={reports.mainstatic}>Общая статистика (Количество)</h1>
          {comps.map((post) => (          
          <p key={post.id} className={reports.text}>{post.name_equipment} - {post.count} штук</p>          
          ))}          
          </main>        
    );
}

export default Reports;
