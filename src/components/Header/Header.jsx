import React from "react";
import LogoImage from "../../images/iv_logo.png";

import header from "./Header.module.scss"

function Header() {

    return (
        <header>
            <nav className={header.top_menu}>
                <a className={header.navbar_logo} href=""><img src={LogoImage} alt=""></img></a>
                <ul className={header.menu_main}>
                    <li><a href="/">Главная</a></li>
                    <li><a href="/inventar">Инвентаризация (основные средства)</a></li>
                    <li><a href="/otherinventar">Инвентаризация (запастные части)</a></li>
                    <li><a href="/spr">Справочники</a></li>
                    <li><a href="/report">Отчеты</a></li>
                    <li><a href="/contact">Контакты</a></li>                    
                </ul>
            </nav>
        </header>
    );
}

export default Header;
