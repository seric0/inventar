import React, { useState, useEffect } from "react";
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Button from '@mui/material/Button';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import Modal from '@mui/material/Modal';
import TabPanel from '@mui/lab/TabPanel';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useSnackbar } from "notistack";
import { FaPlus } from "react-icons/fa";
import { FaPlusCircle } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaSave } from "react-icons/fa";
import { FaEdit } from "react-icons/fa";
import LoadingScreen from "../Loader/Loader";

import maininventar from "./MainReferences.module.scss"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

function MainReferences() {              
    const [value, setValue] = useState('1');
    const [selectedId, setSelectedId] = useState(null);
    const [currentPost, setCurrentPost] = useState(null);    

    const openData = (post) => {       
      setCurrentPost(post);
      setName(post.name_inv);          
      setOpen4(true);
    }

    const openDataSpr = (post) => {       
      setCurrentPost(post);
      setName(post.name_spr);          
      setOpen5(true);
    }

    const openDataCab = (post) => {       
      setCurrentPost(post);
      setName(post.name);          
      setOpen6(true);
    }

    var indexone = 1;
    var indextwo = 1;
    var indexthree = 1;
    var indexfour = 1;
    var indexfive = 1;
    var indexsix = 1;
    var indexseven = 1;
    
    const [loading, setLoading] = useState(true);
        useEffect(() => {
        setTimeout(() => setLoading(false), 2000);
      }, [])           
    
    const handleChange = (event, newValue) => {
    setValue(newValue);}
                                                                                
    const [open, setOpen] = useState(false);
    const [open2, setOpen2] = useState(false);
    const [open3, setOpen3] = useState(false);
    const [open4, setOpen4] = useState(false);
    const [open5, setOpen5] = useState(false);
    const [open6, setOpen6] = useState(false);
    const [open7, setOpen7] = useState(false);
    const [open8, setOpen8] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleOpen2 = () => setOpen2(true);
    const handleOpen3 = () => setOpen3(true);
    const handleOpen4 = () => setOpen4(true);
    const handleOpen5 = () => setOpen5(true);
    const handleOpen6 = () => setOpen6(true);
    const handleOpen7 = () => setOpen7(true);
    const handleOpen8 = () => setOpen8(true);
    const handleClose = () => setOpen(false);
    const handleClose2 = () => setOpen2(false);
    const handleClose3 = () => setOpen3(false);
    const handleClose4 = () => setOpen4(false);
    const handleClose5 = () => setOpen5(false);
    const handleClose6 = () => setOpen6(false);
    const handleClose7 = () => setOpen7(false);    
    const handleClose8 = () => setOpen8(false);
    
    const [name, setName] = useState('');
    const [sprtype, setSprType] = useState('');    

const { enqueueSnackbar } = useSnackbar();

const [spr, setSpr] = useState([]);   
        const fetchDataSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getSprMain.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSpr(data);
            });
        };        
        useEffect(() => {
          fetchDataSpr();
        }, []);

        const [cabinetspr, setCabinetSpr] = useState([]);   
        const fetchCabinetSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setCabinetSpr(data);
            });
        };        
        useEffect(() => {
          fetchCabinetSpr();
        }, []);

        const [frequencyspr, setFrequencySpr] = useState([]);   
        const fetchFrequencySpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getFrequency.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setFrequencySpr(data);
            });
        };        
        useEffect(() => {
          fetchFrequencySpr();
        }, []);

        const [memoryspr, setMemorySpr] = useState([]);   
        const fetchMemorySpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getMemory.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setMemorySpr(data);
            });
        };        
        useEffect(() => {
          fetchMemorySpr();
        }, []);

        const [hddspr, setHddSpr] = useState([]);   
        const fetchHddSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getHdd.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setHddSpr(data);
            });
        };        
        useEffect(() => {
          fetchHddSpr();
        }, []);

        const [ocspr, setOcSpr] = useState([]);   
        const fetchOcSpr = () => {            
          fetch(`https://seric0.kz/API/inventar/getOC.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setOcSpr(data);
            });
        };        
        useEffect(() => {
          fetchOcSpr();
        }, []);

        const [manufacturer, setManufacturer] = useState([]);        
        const fetchSprData = () => {            
            fetch(`http://seric0.kz/API/inventar/getSprManufacturer.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setManufacturer(data);
              });
          };        
          useEffect(() => {
            fetchSprData();
          }, []);

const [sprEquipment, setSprEquipment] = useState([]);   
        const fetchDataSprEquipment = () => {            
          fetch(`https://seric0.kz/API/inventar/getSprMainEquipment.php`)
            .then((response) => {
              return response.json();
            })
            .then((data) => {
              setSprEquipment(data);
            });
        };        
        useEffect(() => {
          fetchDataSprEquipment();
        }, []);

  const saveData = () => {    
  var params = new URLSearchParams();
  const name_new = name.toLowerCase();
  const result = name_new.charAt(0).toUpperCase() + name_new.slice(1); 
  const passed1 = sprEquipment.some(n => n.name === result);
  const passed2 = spr.some(n => n.name === result);
  params.set('name_inv', name);
  params.set('spr_type', sprtype);
  if (passed1 === true || passed2 === true) {
    enqueueSnackbar({
      variant: "error",
      message: "Наименование уже есть!",
    });
  }
  else 
  {  
  if (name !== "" && sprtype !== "") {
  fetch('https://seric0.kz/API/inventar/addSpr.php', {
    method: 'POST',
    body: params
 }).then(
    response => {
       return response.text();       
    }  
 ).then(
    text => {                  
      enqueueSnackbar({
        variant: "success",
        message: "Данные добавлены",
      });
      setTimeout(window.location.reload(), 2000);
      handleClose();       
    }           
 ); 
}
else {
  enqueueSnackbar({
    variant: "error",
    message: "Введите все данные!",
  }); 
}
}
}

const saveDataSpr = () => {
  var params = new URLSearchParams();
  params.set('name_spr', name);
  params.set('spr_type', sprtype);
  if (name !== "" && sprtype !== "") {
    fetch('https://seric0.kz/API/inventar/addCompSpr.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose2();       
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const saveDataCab = () => {
  var params = new URLSearchParams();
  params.set('name', name);
  if (name !== "") {
    fetch('https://seric0.kz/API/inventar/addCabinet.php', {
      method: 'POST',
      body: params
   }).then(
      response => {
         return response.text();       
      }  
   ).then(
      text => {                  
        enqueueSnackbar({
          variant: "success",
          message: "Данные добавлены",
        });
        setTimeout(window.location.reload(), 2000);
        handleClose4();
      }           
   ); 
  }
  else {
    enqueueSnackbar({
      variant: "error",
      message: "Введите все данные!",
    }); 
  }
}

const handleSave = () => {    
  const updatedData = {
    id: currentPost.id,  
    name_inv: name      
  };
  
  fetch(`https://seric0.kz/API/inventar/updateSprInv.php`, {
    method: 'POST',
    body: JSON.stringify(updatedData),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },    
  })
    .then((response) => response.json())
    .then((data) => {
      setTimeout(window.location.reload(), 2000);
      handleClose4();
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });

  // var params = new URLSearchParams();
  // let n = name;
  // const updatedData = {
  //     id: currentPost.id,   // ID записи, которую редактируем
  //     name_inv: n,       // Новое имя, которое введено в input
  //   };    
  //   let id = currentPost.id;         
  //     params.set('name_inv', name);      
  //     if (name !== "") { 
  //     fetch(`https://seric0.kz/API/inventar/updateSpr.php?id=${id}`, {
  //       method: 'POST',
  //       body: params
  //    }).then(
  //       response => {
  //          return response.text();       
  //       }  
  //    ).then(
  //       text => {                  
  //         enqueueSnackbar({
  //           variant: "success",
  //           message: "Данные обновлены",
  //         });
  //         //setTimeout(window.location.reload(), 2000);
  //         handleClose8();       
  //       }           
  //    ); 
  //   }
  //   else {
  //     enqueueSnackbar({
  //       variant: "error",
  //       message: "Введите все данные!",
  //     }); 
  //   }  
  // fetch(`https://seric0.kz/API/inventar/updateSpr.php?id=${updatedData.id}`, {
  //   method: 'POST',  
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify(updatedData),
  // })
  //   .then((response) => {
  //     console.log('Raw response:', response);
  //     return response.text();  // Получаем текст ответа
  //   })
  //   .then((text) => {
  //     try {
  //       const data = JSON.parse(text);  // Пробуем распарсить в JSON
  //       console.log('Success:', data);
  //     } catch (e) {
  //       console.error('Error parsing JSON:', e);
  //       console.log('Raw response text:', text);  // Логируем текст, чтобы увидеть, что пришло
  //     }
  //   })
  //   .catch((error) => {
  //     console.error('Fetch error:', error);
  //   });
};

const handleSaveSpr = () => {    
  const updatedData = {
    id: currentPost.id,  
    name_spr: name      
  };
  
  fetch(`https://seric0.kz/API/inventar/updateCompSpr.php`, {
    method: 'POST',
    body: JSON.stringify(updatedData),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },    
  })
    .then((response) => response.json())
    .then((data) => {
      setTimeout(window.location.reload(), 2000);
      handleClose5();
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });  
};

const handleSaveCab = () => {    
  const updatedData = {
    id: currentPost.id,  
    name: name      
  };
  
  fetch(`https://seric0.kz/API/inventar/updateSprCab.php`, {
    method: 'POST',
    body: JSON.stringify(updatedData),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },    
  })
    .then((response) => response.json())
    .then((data) => {
      setTimeout(window.location.reload(), 2000);
      handleClose5();
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });  
};

const tableSearch = () => {
  var phrase = document.getElementById('search-text');
  var table = document.getElementById('info-table');
  var regPhrase = new RegExp(phrase.value, 'i');
  var flag = false;
  for (var i = 1; i < table.rows.length; i++) {
      flag = false;
      for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
          flag = regPhrase.test(table.rows[i].cells[j].innerHTML);          
          if (flag) break;
      }
      if (flag) {
          table.rows[i].style.display = "";
      } else {
          table.rows[i].style.display = "none";
      }      
  }  
}

    return (                                        
        <div className={maininventar.main}>
          {loading && <LoadingScreen />}            
            <h1 className={maininventar.mainstatic}>Справочники</h1>                                            
                <Button variant="contained" onClick={handleOpen} sx={{ height: '30px'}}><FaPlus /></Button>
                <Button variant="contained" onClick={tableSearch} sx={{ marginLeft: '20px', height: '30px'}}><FaSearch /></Button>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name_inv" id="name_inv" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (типы основных средств)</MenuItem>
                    <MenuItem value="2">Справочник (типы запасных частей)</MenuItem>
                    <MenuItem value="3">Справочник (наименование основных средств)</MenuItem>
                </Select>
                <Button variant="contained" onClick={saveData}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open2}
                    onClose={handleClose2}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose2} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name_manufact" id="name_manufact" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (наименование производителей)</MenuItem>
                    <MenuItem value="2">Справочник (тактовая частота процессоров)</MenuItem>
                    <MenuItem value="3">Справочник (количество оперативной памяти)</MenuItem>
                    <MenuItem value="4">Справочник (объем жестого диска)</MenuItem>
                    <MenuItem value="5">Справочник (наименование операционной системы)</MenuItem>
                </Select>                                
                <Button variant="contained" onClick={saveDataSpr}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open3}
                    onClose={handleClose3}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose3} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                                              
                <Button variant="contained" onClick={saveDataCab}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                {/* <Modal
                    open={open4}
                    onClose={handleClose4}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose4} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (наименование производителей)</MenuItem>
                    <MenuItem value="2">Справочник (тактовая частота процессоров)</MenuItem>
                    <MenuItem value="3">Справочник (количество оперативной памяти)</MenuItem>
                    <MenuItem value="4">Справочник (объем жестого диска)</MenuItem>
                    <MenuItem value="5">Справочник (наименование операционной системы)</MenuItem>
                </Select>                                
                <Button variant="contained" onClick={saveDataSpr}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={open5}
                    onClose={handleClose5}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose5} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (наименование производителей)</MenuItem>
                    <MenuItem value="2">Справочник (тактовая частота процессоров)</MenuItem>
                    <MenuItem value="3">Справочник (количество оперативной памяти)</MenuItem>
                    <MenuItem value="4">Справочник (объем жестого диска)</MenuItem>
                    <MenuItem value="5">Справочник (наименование операционной системы)</MenuItem>
                </Select>                                
                <Button variant="contained" onClick={saveDataSpr}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open6}
                    onClose={handleClose6}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose6} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Объем жесткого диска:</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (наименование производителей)</MenuItem>
                    <MenuItem value="2">Справочник (тактовая частота процессоров)</MenuItem>
                    <MenuItem value="3">Справочник (количество оперативной памяти)</MenuItem>
                    <MenuItem value="4">Справочник (объем жестого диска)</MenuItem>
                    <MenuItem value="5">Справочник (наименование операционной системы)</MenuItem>
                </Select>                                
                <Button variant="contained" onClick={saveDataSpr}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

                <Modal
                    open={open7}
                    onClose={handleClose7}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose7} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />
                <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={sprtype}
                  label="Type"
                  onChange={event => setSprType(event.target.value)}
                >                  
                    <MenuItem value="1">Справочник (наименование производителей)</MenuItem>
                    <MenuItem value="2">Справочник (тактовая частота процессоров)</MenuItem>
                    <MenuItem value="3">Справочник (количество оперативной памяти)</MenuItem>
                    <MenuItem value="4">Справочник (объем жестого диска)</MenuItem>
                    <MenuItem value="5">Справочник (наименование операционной системы)</MenuItem>
                </Select>                                
                <Button variant="contained" onClick={saveDataSpr}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>    */}
                <Modal
                    open={open4}
                    onClose={handleClose4}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose4} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name_inv" id="name_inv" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={handleSave}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open5}
                    onClose={handleClose5}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose5} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">Наименование</label>
                <input type="text" value={name} className={maininventar.textinput} name="name_spr" id="name_spr" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={handleSaveSpr}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>
                <Modal
                    open={open6}
                    onClose={handleClose6}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style}>
                    <Button onClick={handleClose6} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
              <form id="form" className={maininventar.input_forms}>
                <label id="bnt1">№ кабинета</label>
                <input type="text" value={name} className={maininventar.textinput} name="name" id="name" required onChange={event => setName(event.target.value)} />                                
                <Button variant="contained" onClick={handleSaveCab}><FaSave /></Button>                
              </form>                        
                    </Box>
                </Modal>

        <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleChange}>
          <Tab label="Справочник (типы и наименование основных средств)" value="1" sx={{ width: '230px' }}/>
          <Tab label="Справочник (типы запасных частей)" value="2" sx={{ width: '230px' }}/>            
          <Tab label="Справочник (наименование производителей)" value="3" sx={{ width: '230px' }}/>                    
          <Tab label="Справочник (наименование кабинетов)" value="4" sx={{ width: '230px' }}/>
          </TabList>
          <TabList onChange={handleChange}>           
          <Tab label="Справочник (тактовая частота процессоров)" value="5" sx={{ width: '230px' }}/>
          <Tab label="Справочник (количество оперативной памяти)" value="6" sx={{ width: '230px' }}/>
          <Tab label="Справочник (объем жестого диска)" value="7" sx={{ width: '230px' }}/>          
          <Tab label="Справочник (наименование операционной системы)" value="8" sx={{ width: '230px' }}/>
          </TabList>
        </Box>
        <TabPanel value="1">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>           
        {spr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>              
                <th>Действие</th>              
            </tr>                                    
            {spr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexone++}</td>
                <td id="allname_1">{post.name_inv}</td>
                <Button variant="contained" onClick={() =>{openData(post)}}><FaEdit /></Button>                
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="2">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>           
        {sprEquipment.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>              
            </tr>                                    
            {sprEquipment.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indextwo++}</td>
                <td id="allname_2">{post.name_inv}</td>
                <Button variant="contained" onClick={() =>{openData(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="3">                
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>          
        <Button variant="contained" onClick={handleOpen2} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button> 
        {manufacturer.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>              
            </tr>                                    
            {manufacturer.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexthree++}</td>
                <td id="allname_2">{post.name_spr}</td>
                <Button variant="contained" onClick={() =>{openDataSpr(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="4">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите номер кабинета" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen3} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>            
        {cabinetspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Номер кабинета</th>              
            </tr>                                    
            {cabinetspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{post.id}</td>
                <td id="allname_2">{post.name}</td>
                <Button variant="contained" onClick={() =>{openDataCab(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="5">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите тактовую частоту в цифрах" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen4} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {frequencyspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Тактовая частота</th>              
            </tr>                                    
            {frequencyspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexfour++}</td>
                <td id="allname_2">{post.name_spr} ГГц</td>
                <Button variant="contained" onClick={() =>{openDataSpr(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="6">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите количество в цифрах" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen5} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {memoryspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Количество оперативной памяти</th>              
            </tr>                                    
            {memoryspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexfive++}</td>
                <td id="allname_2">{post.name_spr > 100 ? post.name_spr + " Мб" : post.name_spr + " Гб"} </td>
                <Button variant="contained" onClick={() =>{openDataSpr(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="7">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите количество в цифрах" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen6} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {hddspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Объем жесткого диска</th>              
            </tr>                                    
            {hddspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexsix++}</td>
                <td id="allname_2">{post.name_spr > 100 ? post.name_spr + " Гб" : post.name_spr + " Тб"} </td>
                <Button variant="contained" onClick={() =>{openDataSpr(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )} 
        </TabPanel>
        <TabPanel value="8">
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите наименование" className={maininventar.searchinput}/>
        </label>
        <Button variant="contained" onClick={handleOpen7} sx={{ height: '30px', marginLeft: '20px'}}><FaPlusCircle /></Button>           
        {ocspr.length > 0 && (        
        <table id="info-table">
            <tr>
                <th>№ п/п</th>
                <th>Наименование операционной системы</th>              
            </tr>                                    
            {ocspr.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexseven++}</td>
                <td id="allname_2">{post.name_spr} </td>
                <Button variant="contained" onClick={() =>{openDataSpr(post)}}><FaEdit /></Button>     
            </tr>            
            ))}                      
        </table>        
        )}
        </TabPanel>
      </TabContext>
    </Box>
    </div>        
    );
}

export default MainReferences;
