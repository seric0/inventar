import React, { useEffect, useState } from "react";
import LoadingScreen from "../Loader/Loader";

import contacts from "./Contacts.module.scss"

function Contacts() {        
    const [inventar_contacts, setContacts] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => setLoading(false), 2000);
      }, [])         
        const fetchData = () => {            
            fetch(`http://seric0.kz/API/inventar/getContacts.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setContacts(data);
              });
          };        
          useEffect(() => {
            fetchData();
          }, []);

    return ( 
        <contacts>               
            <main className={contacts.main}>
            <h1 className={contacts.mainstatic}>Контакты</h1>            
            {inventar_contacts.map((contact) => (
            <div className={contacts.maintext}>
            <p className={contacts.text}>Наименование - {contact.name}</p>            
            <p className={contacts.text}>Адрес - {contact.area}</p>
            <p className={contacts.text}>Программист - {contact.personal}</p>
            <p className={contacts.email}>E-mail: <a href="mailto:{contact.email}">{contact.email}</a></p>
            </div>             
            ))}
            </main>
        </contacts>                    
    );
}

export default Contacts;
