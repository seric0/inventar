import React from "react";

import footer from "./Footer.module.scss"

function Footer() {

    const login = localStorage.getItem("fio");

    const exitUser = () => { 
        localStorage.removeItem("fio");
        setTimeout(window.location.reload(), 2000);
    }

    return (
        <footer className={footer.top_menu}>            
            <p className={footer.text}>Ввод данных осуществляет: 00NR Коммунальное государственное предприятие "Поликлиника № 1 города Костанай" Управления здравоохранения акимата Костанайской области</p>            
            <p className={footer.login}>Вы вошли как: {login} <button onClick={exitUser} className={footer.exit_btn}>Выйти</button></p>                                                                         
            <p>Версия: 2024 1.1</p>            
        </footer>
    );
}

export default Footer;
