import React from "react";

import loader from "./Loader.module.scss"

function Loader() {        
    return (
        <loader>
            <div className={loader.loading_screen}>
            <div className={loader.loading_spinner}></div>
            </div>
        </loader>
    );
}

export default Loader;
