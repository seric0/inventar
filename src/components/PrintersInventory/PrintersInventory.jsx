import React, { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import TabPanel from '@mui/lab/TabPanel';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import { FaEdit } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaSave } from "react-icons/fa";
import { FaFileExcel } from "react-icons/fa";
import Stack from '@mui/material/Stack';
import Pagination from '@mui/material/Pagination';
import { useSnackbar } from "notistack";
import * as XLSX from 'xlsx';

import printersinventar from "./PrintersInventory.module.scss"

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  padding: '33px',
};

function PrintersInventory() {        

        var indexfive = 1;
        const [open, setOpen] = useState(false);                    
        const [currentPostInv, setCurrentPostInv] = useState(null);
        const [searchparam, setSearchParam] = useState('2');
        const [printers, setPrinters] = useState([]);
        const [page, setPage] = useState(1);
        const [filteredPrinters, setFilteredPrinters] = useState([]); 
        const [searchtext, setSearchText] = useState(''); 
        const handleClose = () => setOpen(false);
        const [openInv, setOpenInv] = useState(false); 
        const handleCloseInv = () => setOpenInv(false);
        const [name, setName] = useState('');
        const [manufacturer_id, setManufacturer] = useState('');
        const [inv, setInv] = useState('');
        const [serial, setSerial] = useState('');
        const [cab, setCab] = useState('');
        const [inv_count, setCount] = useState('');
        const [invtype, setInvType] = useState('');
        const [model, setModel] = useState('');
        const [work, setWork] = useState('');
        const [status, setStatus] = useState('');

        const { enqueueSnackbar } = useSnackbar();
        
          const fetchDataPrinter = () => {            
            fetch(`http://seric0.kz/API/inventar/getInventar_printer.php`)
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                setPrinters(data);
              });
          };        
          useEffect(() => {
            fetchDataPrinter();
          }, []); 

  const [names, setNames] = useState([]);
  const fetchDataSprName = () => {
    fetch(`http://seric0.kz/API/inventar/getSprNames.php`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setNames(data);
      });
  };
  useEffect(() => {
    fetchDataSprName();
  }, []);

  const [spr_manufacturer, setSprManufacturer] = useState([]);
            const fetchDataSprManufacturer = () => {
              fetch(`https://seric0.kz/API/inventar/getSprManufacturer.php`)
                .then((response) => {
                  return response.json();
                })
                .then((data) => {
                  setSprManufacturer(data);
                });
            };
            useEffect(() => {
              fetchDataSprManufacturer();
            }, []);

          const [cabinet, setCabinet] = useState([]);
            const fetchDataCab = () => {
              fetch(`https://seric0.kz/API/inventar/getCabinet.php`)
                .then((response) => {
                  return response.json();
                })
                .then((data) => {
                  setCabinet(data);
                });
            };
            useEffect(() => {
              fetchDataCab();
            }, []);          
          
            const [spr, setSpr] = useState([]);
            const fetchDataSpr = () => {
              fetch(`http://seric0.kz/API/inventar/getSprMain.php`)
                .then((response) => {
                  return response.json();
                })
                .then((data) => {
                  setSpr(data);
                });
            };
            useEffect(() => {
              fetchDataSpr();
            }, []);

            const saveDataInv = () => { 
              const currentDate = new Date();
              const date_inv = currentDate.toISOString().slice(0, 19).replace('T', ' '); 
        
              const updatedData = {
                id: currentPostInv.id,  
                name_inventar_id: name,        
                model: model,
                inv_number: inv,
                serial_number: serial,
                cabinet: cab,
                manufacturer_id: manufacturer_id,
                inventar_type: invtype,
                work: work,
                status: status,
                date_inv: date_inv   
              };
                     
              fetch(`https://seric0.kz/API/inventar/updateInventar.php`, {
                method: 'PUT',
                body: JSON.stringify(updatedData),
                headers: {
                  'Content-type': 'application/json; charset=UTF-8',
                },    
              })
                .then((response) => response.json())
                .then(() => {                        
                    enqueueSnackbar({
                      variant: "success",
                      message: "Данные обновлены",
                    });
                  //setTimeout(window.location.reload(), 4000);          
                  fetchDataPrinter();          
                  handleCloseInv();        
                })
                .catch((error) => {
                  enqueueSnackbar({
                    variant: "error",
                    message: "Ошибка при обновлении "+error,
                  })
                });      
            };

            const openInvData = (post) => {
              setCurrentPostInv(post);
              setName(post.name_inventar_id);
              setManufacturer(post.manufacturer_id);
              setModel(post.model);                
              setInv(post.inv_number);
              setSerial(post.serial_number);
              setCab(post.cabinet);                        
              setInvType(post.inventar_type);
              setWork(post.work);
              setStatus(post.status);                        
              setOpenInv(true);
            }

            const exportToExcel = () => {
                          var excelindex = 1;
                          const headers = ['№ п/п', 'Наименование', 'Модель', 'Инвентарный номер', 'Серийный номер', '№ кабинета', 'Статус работоспособности', 'Причина'];                      
                          const data = printers.map(item => [excelindex++, item.name_inv, item.name_spr+' '+item.model,item.inv_number, item.serial_number,item.name, item.work, item.status]);                        
                          const ws = XLSX.utils.aoa_to_sheet([headers, ...data]);          
                          const wb = XLSX.utils.book_new();
                          XLSX.utils.book_append_sheet(wb, ws, 'Копьютеры');          
                          XLSX.writeFile(wb, 'data.xlsx');
                        };
          
                    const [postPerPage] = useState(10);  
                    const lastPost = page * postPerPage;
                    const firstPost = lastPost - postPerPage;          
                    const currentPost = filteredPrinters.slice(firstPost, lastPost);
                    const PageCount = Math.ceil(filteredPrinters.length / postPerPage);          
                    const ChangePage = ( event, value ) => {                                        
                      setPage(value);          
                    };
          
                    const findData = () => {                                                                                                                               
                      if (searchtext) {
                        let filtered = [];

                        if (searchparam === '1') {
                          filtered = printers.filter((printer) => printer.name_inv.toLowerCase().includes(searchtext.toLowerCase()));
                        }
                        
                        if (searchparam === '2') {
                          filtered = printers.filter((printer) => printer.inv_number.includes(searchtext));
                        }
                      
                        if (searchparam === '3') {
                          filtered = printers.filter((printer) => printer.serial_number.includes(searchtext));
                        }

                        if (searchparam === '4') {
                          filtered = printers.filter((printer) => printer.name.includes(searchtext));
                        }
                        
                        setFilteredPrinters(filtered);
                      } else {
                        setFilteredPrinters(printers);
                      }
                    }
          
                    useEffect(() => {
                    findData();
                    }, [printers]); 
                            
    return (                                        
        <div className={printersinventar.main}>                                 
        <TabPanel value="5" sx={{ width: '1024px' }}>
        <label htmlFor="search-text">
        Поиск:
        <input id="search-text" type="text" placeholder="Введите инвентарный номер" className={printersinventar.searchinput} value={searchtext} onChange={event => setSearchText(event.target.value)}/>
        </label>
        <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={searchparam}
                  label="Type"
                  sx={{ marginLeft: '15px' }}
                  onChange={event => setSearchParam(event.target.value)}
                >                  
                    <MenuItem value="1">Наименование</MenuItem>
                    <MenuItem value="2">Инвентарный номер</MenuItem>
                    <MenuItem value="3">Серийный номер</MenuItem>
                    <MenuItem value="4">№ кабинета</MenuItem>                    
        </Select>
        <Button variant="contained" title="Экспорт в excel" onClick={exportToExcel} sx={{ marginLeft: '20px', height: '30px'}}><FaFileExcel /></Button>
        <Button variant="contained" title="Поиск" onClick={findData} sx={{ marginLeft: '20px', height: '30px' }}><FaSearch /></Button>
        <Modal
          open={openInv}
          onClose={handleCloseInv}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description">
          <Box sx={style}>
            <Button onClick={handleCloseInv} variant="contained" sx={{ marginLeft: '90%', marginTop: '-25px', marginBottom: '20px' }}>X</Button>
            <form id="form" className={printersinventar.input_forms}>
              <label>Наименование:</label>              
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={name}
                label="Type"
                onChange={event => setName(event.target.value)}
              >
                {names.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <InputLabel id="demo-simple-select-label">Производитель:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={manufacturer_id}
                label="Type"
                onChange={event => setManufacturer(event.target.value)}
              >
                {spr_manufacturer.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_spr}</MenuItem>
                ))}
              </Select>
              <label>Модель:</label>
              <input type="text" value={model} className={printersinventar.textinput} name="model" id="model" required onChange={event => setModel(event.target.value)} />
              <label>Инвентарный номер:</label>             
              <input type="text" value={inv} className={printersinventar.textinput} name="inv_number" id="inv_number" required onChange={event => setInv(event.target.value)} />              
              <label>Серийный номер:</label>
              <input type="text" value={serial} className={printersinventar.textinput} name="serial_number" id="serial_number" required onChange={event => setSerial(event.target.value)} />
              <label>№ кабинета:</label>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={cab}
                  label="Type"
                  onChange={event => setCab(event.target.value)}
                >
                  {cabinet.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name}</MenuItem>
                  ))}
                </Select>
              <InputLabel id="demo-simple-select-label">Тип:</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={invtype}
                label="Type"
                onChange={event => setInvType(event.target.value)}
              >
                {spr.map((post) => (
                  <MenuItem key={post.id} value={post.id}>{post.name_inv}</MenuItem>
                ))}
              </Select>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={work}
                  label="Type"
                  onChange={event => setWork(event.target.value)}
                >
                  <MenuItem value="Работает">Работает</MenuItem>
                  <MenuItem value="Не работает">Не работает</MenuItem>
                </Select>
                <label>Причина:</label>
                            <textarea value={status} className={printersinventar.textinput} name="status" id="status" required onChange={event => setStatus(event.target.value)}/>  
              <Button variant="contained" title="Сохранить" onClick={saveDataInv} sx={{ height: '30px' }}><FaSave /></Button>
            </form>
          </Box>
        </Modal>
        <Stack spacing={2}>         
        {printers.length > 0 && (        
        <table id="info-table">
          <thead>
            <tr>
                <th>№ п/п</th>
                <th>Наименование</th>
                <th>Модель</th>
                <th>Инвентарный номер</th>
                <th>Серийный номер</th>
                <th>№ кабинета</th>
                <th>Статус работоспособности</th>
                <th>Причина</th>
            </tr>
          </thead>
            <tbody id="tbody">                                    
            {currentPost.map((post) => (
            <tr id="tr" key={post.id}>                               
                <td>{indexfive++}</td>
                <td>{post.name_inv || 'Не указано'}</td>                                                
                <td>{post.name_spr+' '+post.model || 'Не указано'}</td>
                <td>{post.inv_number || 'Не указано'}</td>
                <td>{post.serial_number || 'Не указано'}</td>
                <td>{post.name || 'Не указано'}</td>
                <td>{post.work || 'Не указано'}</td>
                <td>{post.status || 'Не указано'}</td>
                <td><Button variant="contained" title="Редактировать" onClick={() =>{openInvData(post)}}><FaEdit /></Button></td>                
            </tr>            
            ))}
            </tbody>                      
        </table>        
        )}
        </Stack>
        <Pagination
        count={PageCount}        
        showFirstButton
        showLastButton               
        onChange={ChangePage}                 
        disableInitialCallback={true}        
        initialPage={1}
        color="primary"
        size="large"        
      /> 
      </TabPanel>
    </div>        
    );
}

export default PrintersInventory;
