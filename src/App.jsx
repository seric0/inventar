import React from "react";
import Router from "./Router"
import { SnackbarProvider } from "notistack";

import "./styles/main.scss";

function App() {
  return (  
    <SnackbarProvider>
  <Router />
  </SnackbarProvider>  
  );
}

export default App;
